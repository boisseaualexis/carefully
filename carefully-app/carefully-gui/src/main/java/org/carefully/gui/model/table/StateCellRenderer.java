package org.carefully.gui.model.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class StateCellRenderer extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = 6420479224535032392L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,	row, column);

		String state = (String) value;
		setText(state.toString());

		switch (state) {
		case "normal":
			setBackground(Color.GREEN);
			break;
		case "attention":
			setBackground(Color.ORANGE);
			break;
		case "alerte":
			setBackground(Color.RED);
			break;
		case "panne":
			setBackground(Color.MAGENTA);
			break;
		case "non installe":
			setBackground(Color.GREEN);
			break;
		case "desinstalle":
			setBackground(Color.GRAY);
			break;

		default:
			setBackground(Color.LIGHT_GRAY);
			break;
		}

		return this;
	}

}
