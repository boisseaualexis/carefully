package org.carefully.gui.app.controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import org.carefully.commons.bean.Sensor;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.gui.app.view.InstallSensorFrame;
import org.carefully.gui.app.view.RoomInfoFrame;
import org.carefully.gui.config.SocketClientRequestor;
import org.carefully.gui.service.ClientSensorService;

public class ControllerRoomInfoFrame implements ActionListener {
	
	private static final String QUESTION_UNISTALL = "Confirmez-vous la  désinstallation de ce capteur : ";
	private static final String TITLE_UNISTALL = "Confirmer ?";
	private static final String COLUMN_MAC_ADDRESS = "Adresse MAC";
	private static final String INSTRUCTION_NO_CHOICE = "Veuillez choisir un capteur dans la liste";
	private static final String TITLE_NO_CHOICE = "Info: Pas de capteur choisi !";
	private static final String TITLE_UNINSTALLATION = "Info: Désinstallation";
	private static final String PB_UNINSTALLATION = "Problème, le capteur n'a pas pu se désinstaller.";
	private static final String SENSOR_UNINSTALLED = "Capteur bien désinstallé.";
	private static final String UNINSTALL_END_RESPONSE = "uninstall-sensor-end";
	
	private Point mouse;
	private RoomInfoFrame rif;
	private String room;
	private String mac_address;
	
	public ControllerRoomInfoFrame(String room, Point mouse, RoomInfoFrame rif) {
		this.room = room;
		this.mouse = mouse;
		this.rif = rif;
		this.rif.getInstallSensor().addActionListener(this);
		this.rif.getExit().addActionListener(this);
		this.rif.getUninstallSensor().addActionListener(this);
		this.rif.getLastUpdate().setText(ClientSensorService.getInstance().getLastUpdate()+" > Dernière mise à jour des données");
	};
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(rif.getInstallSensor())) {
			InstallSensorFrame isf = new InstallSensorFrame(room);
			ControllerInstallSensorFrame controllerRif = new ControllerInstallSensorFrame(room, mouse, isf);
		}
		if(e.getSource().equals(rif.getExit())) {
			rif.dispose();			
		}
		if(e.getSource().equals(rif.getUninstallSensor())) {
			int rowIndex = rif.getSiTable().getSelectedRow();
			int columnIndex = rif.getSiModel().findColumn(COLUMN_MAC_ADDRESS);
			if (rowIndex>=0) {
				mac_address = rif.getSiModel().getValueAt(rowIndex, columnIndex).toString();
				Object[] options = {"Annuler",
                "Confirmer"};
				int n = JOptionPane.showOptionDialog(rif,
						QUESTION_UNISTALL+mac_address,
						TITLE_UNISTALL,
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[0]);
				if (n==1) {
					boolean success = uninstallSensor();
					if (success) {
							rif.getSiModel().deleteRowAt(rowIndex);
					}
				}
			}
			else {
				JOptionPane.showMessageDialog(rif, INSTRUCTION_NO_CHOICE, TITLE_NO_CHOICE, JOptionPane.WARNING_MESSAGE);
			}
		}

	}

	private boolean uninstallSensor() {
		boolean unistalled = false;
		Sensor sensorToUninstall = new Sensor();
		sensorToUninstall.setMac_address(mac_address);
		try {
			Socket sck = SocketClientRequestor.instance().getConnection();
			DataInputStream is = new DataInputStream(sck.getInputStream());
	         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
	         
			 Request request = new Request();
			 request.setRequest("uninstall-sensor");
			 request.setObject(sensorToUninstall);
			 String jsonRequest = SerializationDeserialization.serialization(request);
			 os.writeUTF(jsonRequest);
			 
			 String jsonResponse = is.readUTF();
			 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse);
			 String message = PB_UNINSTALLATION;
			 int messageType = JOptionPane.ERROR_MESSAGE;
			 if (finalResponse.getSuccess() && finalResponse.getRequest().equalsIgnoreCase(UNINSTALL_END_RESPONSE)) {
				 System.out.println(SENSOR_UNINSTALLED);
				 message = SENSOR_UNINSTALLED;
				 messageType = JOptionPane.INFORMATION_MESSAGE;
				 unistalled = true;
			 } else {
				 System.out.println(PB_UNINSTALLATION);
				 message = PB_UNINSTALLATION;
				 messageType = JOptionPane.ERROR_MESSAGE;
				 unistalled = false;
			 }
			 JOptionPane.showMessageDialog(rif, message, TITLE_UNINSTALLATION, messageType);
		} catch (UnknownHostException exc) {
	         exc.printStackTrace();
		} catch (IOException exc) {
	         exc.printStackTrace();
	    }
		return unistalled;
		
	}
	
}
