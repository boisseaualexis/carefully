package org.carefully.gui.model.combobox;

import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

public abstract class AbstractStringModel<T> extends DefaultComboBoxModel<String> {

	private static final long serialVersionUID = 5120749119674648909L;
	protected ArrayList<String> strings;

	public abstract void setStrings();

	public AbstractStringModel(){
		super();
		setStrings();
	}
 
	/*public StringModel(String[] strings){
		super();
 
		this.strings = new ArrayList<String>();
 
		for(String string : strings){
			this.strings.add(string);
		}
	}*/
 
	protected ArrayList<String> getStrings(){
		return strings;
	}
 
	public String getSelectedString(){
		return (String)getSelectedItem();
	}
 
	@Override
	public String getElementAt(int index) {
		return strings.get(index);
	}
 
	@Override
	public int getSize() {
		return strings.size();
	}
 
	@Override
	public int getIndexOf(Object element) {
		return strings.indexOf(element);
	}
}
