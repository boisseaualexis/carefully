package org.carefully.gui.app.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import org.carefully.commons.bean.Sensor;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.gui.app.view.SensorInfoFrame;
import org.carefully.gui.config.SocketClientRequestor;
import org.carefully.gui.service.ClientSensorService;

public class ControllerSensorInfoFrame implements ActionListener {
	
	//private static final String COLUMN_MAC_ADDRESS = "Adresse MAC";
	private static final String QUESTION_UNISTALL = "Confirmez-vous la  désinstallation de ce capteur : ";
	private static final String TITLE_UNISTALL = "Confirmer ?";
	private static final String TITLE_UNINSTALLATION = "Info: Désinstallation";
	private static final String PB_UNINSTALLATION = "Problème, le capteur n'a pas pu se désinstaller.";
	private static final String SENSOR_UNINSTALLED = "Capteur bien désinstallé.";
	private static final String UNINSTALL_END_RESPONSE = "uninstall-sensor-end";
	
	//private Point mouse;
	private SensorInfoFrame sif;
	private Sensor sensor;
	//private String mac_address;
	
	public ControllerSensorInfoFrame(Sensor sensor, SensorInfoFrame sif) {
		this.sensor = sensor;
		this.sif = sif;
		this.sif.getExit().addActionListener(this);
		this.sif.getUninstallSensor().addActionListener(this);
		this.sif.getLastUpdate().setText(ClientSensorService.getInstance().getLastUpdate()+" > Dernière mise à jour des données");
	};
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(sif.getExit())) {
			sif.dispose();			
		}
		if(e.getSource().equals(sif.getUninstallSensor())) {
			//int rowIndex = 0;
			Object[] options = {"Annuler",
                    "Confirmer"};
			int n = JOptionPane.showOptionDialog(sif,
					QUESTION_UNISTALL+sensor.getMac_address(),
					TITLE_UNISTALL,
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[0]);
			//JOptionPane.showMessageDialog(sif, INSTRUCTION_NO_CHOICE, TITLE_NO_CHOICE, JOptionPane.YES_NO_OPTION);
			if (n==1) {
				boolean success = uninstallSensor();
				if (success) {
					//sif.getSiModel().deleteRowAt(rowIndex);
					sif.dispose();
				}
			}
			/*else {
				JOptionPane.showMessageDialog(sif, INSTRUCTION_NO_CHOICE, TITLE_NO_CHOICE, JOptionPane.INFORMATION_MESSAGE);
			}*/
		}

	}

	private boolean uninstallSensor() {
		boolean unistalled = false;
		Sensor sensorToUninstall = sensor;
		try {
			Socket sck = SocketClientRequestor.instance().getConnection();
			DataInputStream is = new DataInputStream(sck.getInputStream());
	         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
	         
			 Request request = new Request();
			 request.setRequest("uninstall-sensor");
			 request.setObject(sensorToUninstall);
			 String jsonRequest = SerializationDeserialization.serialization(request);
			 os.writeUTF(jsonRequest);
			 
			 String jsonResponse = is.readUTF();
			 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse);
			 String message = PB_UNINSTALLATION;
			 int messageType = JOptionPane.ERROR_MESSAGE;
			 if (finalResponse.getSuccess() && finalResponse.getRequest().equalsIgnoreCase(UNINSTALL_END_RESPONSE)) {
				 System.out.println(SENSOR_UNINSTALLED);
				 message = SENSOR_UNINSTALLED;
				 messageType = JOptionPane.INFORMATION_MESSAGE;
				 unistalled = true;
			 } else {
				 System.out.println(PB_UNINSTALLATION);
				 message = PB_UNINSTALLATION;
				 messageType = JOptionPane.ERROR_MESSAGE;
				 unistalled = false;
			 }
			 JOptionPane.showMessageDialog(sif, message, TITLE_UNINSTALLATION, messageType);
		} catch (UnknownHostException exc) {
	         exc.printStackTrace();
		} catch (IOException exc) {
	         exc.printStackTrace();
	    }
		return unistalled;
		
	}
	
}
