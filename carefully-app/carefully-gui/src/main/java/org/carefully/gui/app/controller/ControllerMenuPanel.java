package org.carefully.gui.app.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.carefully.gui.app.view.AllInstalledSensorsFrame;
import org.carefully.gui.app.view.MenuPanel;

public class ControllerMenuPanel implements ActionListener {
	
	MenuPanel pMenu;

	public ControllerMenuPanel(MenuPanel pMenu) {
		super();
		this.pMenu = pMenu;
		this.pMenu.getQuit().addActionListener(this);
		this.pMenu.getShowInstalledSensors().addActionListener(this);
		this.pMenu.getShowNotInstalledSensors().addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(pMenu.getQuit())) {
			System.exit(0);
		}
		if(e.getSource().equals(pMenu.getShowInstalledSensors())) {
			AllInstalledSensorsFrame aisf = new AllInstalledSensorsFrame();
		}
		if(e.getSource().equals(pMenu.getShowNotInstalledSensors())) {
			AllNotInstalledSensorsFrame aNotisf = new AllNotInstalledSensorsFrame();
		}
	}

}
