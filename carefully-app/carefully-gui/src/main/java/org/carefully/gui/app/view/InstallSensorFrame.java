package org.carefully.gui.app.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.carefully.gui.model.combobox.SensorTypeStringModel;
import org.carefully.gui.model.table.SensorsInstallModel;
import org.carefully.gui.model.table.StateCellRenderer;
import org.carefully.gui.model.table.TypeCellRenderer;

public class InstallSensorFrame extends JFrame {
	
	private static final long serialVersionUID = -4043384510147069005L;
	
	private JButton validate;
	private JComboBox<String> stTypes;
	private SensorTypeStringModel stModel;
	private JTable siTable;
	private SensorsInstallModel siModel;
	private JScrollPane scrollPane;

	public InstallSensorFrame(String room) {
		super(room);
		validate = new JButton("Ajouter le capteur choisi");
		
		this.setLocation(500,150);
		this.setSize(500, 400);
		this.setLayout(new BorderLayout(5, 5));
		
		this.stModel = new SensorTypeStringModel();
		this.stTypes = new JComboBox<String>(stModel);
		stTypes.setSelectedItem(stModel.getInstruction());
		this.add(stTypes, BorderLayout.NORTH);
		
		siModel = new SensorsInstallModel(stModel.getSelectedString());
		siTable = new JTable(siModel);
		siTable.setAutoCreateRowSorter(true);
		siTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		siTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		siTable.getColumnModel().getColumn(2).setCellRenderer(new TypeCellRenderer());
		siTable.getColumnModel().getColumn(0).setCellRenderer(new StateCellRenderer());
		siTable.getColumnModel().getColumn(1).setPreferredWidth(250);
		scrollPane = new JScrollPane(siTable);
		scrollPane.setPreferredSize(new Dimension(500,200));
		this.add(scrollPane, BorderLayout.CENTER);
		
		this.add(validate, BorderLayout.SOUTH);
		this.setBackground(Color.GRAY);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	public JButton getValidate() {
		return validate;
	}
	
	public JComboBox<String> getStTypes() {
		return stTypes;
	}
	
	public SensorTypeStringModel getStModel() {
		return stModel;
	}
	
	public JTable getSiTable() {
		return siTable;
	}
	
	public SensorsInstallModel getSiModel() {
		return siModel;
	}
	
	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	/*public JButton getCancel() {
		return cancel;
	}*/

}
