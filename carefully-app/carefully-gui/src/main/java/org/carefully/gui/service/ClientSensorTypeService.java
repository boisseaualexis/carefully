package org.carefully.gui.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.carefully.commons.bean.SensorType;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.gui.config.SocketClientRequestor;

public class ClientSensorTypeService {
	
	private static ClientSensorTypeService instance;
	
	private List<SensorType> sensorTypes;

	private ClientSensorTypeService() {
		selectAllSensorTypes();
	}
	
	private void selectAllSensorTypes() {
		List<SensorType> listSensorTypes = new ArrayList<SensorType>();
		try {
			Socket sck = SocketClientRequestor.instance().getConnection();
			DataInputStream is = new DataInputStream(sck.getInputStream());
	         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
	         
			 Request request = new Request();
			 request.setRequest("select-sensor-type-all");
			 String jsonRequest = SerializationDeserialization.serialization(request);
			 os.writeUTF(jsonRequest);
			 
			 String jsonResponse = is.readUTF();
			 Request response = SerializationDeserialization.deserialization(jsonResponse);
			 SensorType sensorType = new SensorType();
			 
			 while (response.getRequest().startsWith("sensor-type-next")) {
				 sensorType = SerializationDeserialization.deserialization(response.getObject(), sensorType);
				 listSensorTypes.add(sensorType);
				 jsonResponse = is.readUTF();
				 response = SerializationDeserialization.deserialization(jsonResponse);
			 }
			 
			 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse);
			 if (finalResponse.getSuccess()) {
				 this.sensorTypes = listSensorTypes;
				 System.out.println("Tous les types de capteur sont récupérés.");
			 } else {
				 System.out.println("Problème, la liste des types de capteur n'est pas disponible.");
			 }
	      } catch (UnknownHostException exc) {
	         exc.printStackTrace();
	      } catch (IOException exc) {
	         exc.printStackTrace();
	      }
		
	}

	public static synchronized ClientSensorTypeService getInstance() {
		if (instance == null) {
			instance = new ClientSensorTypeService();
		}
		return instance;
	}

	public synchronized List<SensorType> findLastSensorTypeInfo() {
		//selectAllSensorTypes();
		return sensorTypes;
	}
}
