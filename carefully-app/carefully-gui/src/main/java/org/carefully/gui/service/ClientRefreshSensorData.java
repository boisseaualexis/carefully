package org.carefully.gui.service;

public class ClientRefreshSensorData implements Runnable {

	private static final long REFRESH_INTERVAL_MS = 4000;
	private Boolean isRunning = true;
	
	

	public ClientRefreshSensorData() {
		super();
	}



	@Override
	public void run() {
		while(isRunning == true){
			try {
				
				Thread.sleep(REFRESH_INTERVAL_MS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Refresh sensor data");
			ClientSensorService.getInstance().RefreshSensorInfo();
			System.out.println("Refresh sensor data done");
		}

	}

}
