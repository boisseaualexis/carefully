package org.carefully.gui.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SocketClientConfig {

    static private SocketClientConfig _instance = null;
    static private String port = null;
    static private String server = null;
	 
    private SocketClientConfig(){
    	InputStream file = null;
    	Properties props = new Properties();

	    try{
	        file = getClass().getResourceAsStream("/socketclient.properties");
	        props.load(file);
	        server = props.getProperty("SERVER");
	        port = props.getProperty("PORT");
	       } 
	    catch(Exception e){
	        System.out.println("error" + e);
	       }
	    finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					file=null;
				}
			}
		}
    }
	
	static public SocketClientConfig instance(){
        if (_instance == null) {
            _instance = new SocketClientConfig();
        }
        return _instance;
    }
	
    public static String getPort() {
		return port;
	}

	public static String getServer() {
		return server;
	}
}