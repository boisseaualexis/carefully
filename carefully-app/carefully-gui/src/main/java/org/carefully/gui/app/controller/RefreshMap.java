package org.carefully.gui.app.controller;

public class RefreshMap implements Runnable {
	
	private static final long REFRESH_INTERVAL_MS = 3000;
	private Boolean isRunning = true;
	private ControllerMapPanel cmp;
	
	

	protected RefreshMap(ControllerMapPanel cmp) {
		super();
		this.cmp = cmp;
	}



	@Override
	public void run() {
		while(isRunning == true){
			try {
				
				Thread.sleep(REFRESH_INTERVAL_MS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Refresh map");
			cmp.repaintMap();
			System.out.println("Refresh map");
		}

	}

}
