package org.carefully.gui.app.controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import javax.swing.JOptionPane;

import org.carefully.commons.bean.Sensor;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.gui.app.view.InstallSensorFrame;
import org.carefully.gui.config.SocketClientRequestor;

public class ControllerInstallSensorFrame implements ActionListener, ItemListener {

	private static final String TITLE_INSTALLATION = "Info: Installation";
	private static final String PB_INSTALLATION = "<html>Problème, le capteur n'a pas pu s'installer pour la ou les raisons suivantes :";
	private static final String SENSOR_INSTALLED = "Capteur bien installé.";
	private static final String COLUMN_MAC_ADDRESS = "Capteurs disponibles en stock";
	private static final String INSTRUCTION_NO_CHOICE = "Veuillez choisir un capteur dans la liste";
	private static final String TITLE_NO_CHOICE = "Info: Pas de capteur choisi !";
	private static final String INSTALL_END_RESPONSE = "install-sensor-end";
	
	private Point mouse;
	private InstallSensorFrame isf;
	private String room;
	private String mac_address;
	
	public ControllerInstallSensorFrame(String room, Point mouse, InstallSensorFrame isf) {
		this.room = room;
		this.mouse = mouse;
		this.isf = isf;
		this.isf.getValidate().addActionListener(this);
		this.isf.getStTypes().addItemListener(this);
		//this.isf.getCancel().addActionListener(this);
	};
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(isf.getValidate())) {
			int rowIndex = isf.getSiTable().getSelectedRow();
			int columnIndex = isf.getSiModel().findColumn(COLUMN_MAC_ADDRESS);
			if (rowIndex>=0) {
				mac_address = isf.getSiModel().getValueAt(rowIndex, columnIndex).toString();
				installSensor();
			}
			else {
				JOptionPane.showMessageDialog(isf, INSTRUCTION_NO_CHOICE, TITLE_NO_CHOICE, JOptionPane.INFORMATION_MESSAGE);
			}
		}
		/*else if(e.getSource().equals(isf.getCancel())) {
			isf.dispose();			
		}*/
	}

	@Override
	public void itemStateChanged(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
	          Object item = event.getItem();
	          isf.getSiModel().initialize((String) item);;
		}		
	}
	
	
	private void installSensor() {
		Sensor sensorToInstall = new Sensor();
		sensorToInstall.setFk_room(room);
		sensorToInstall.setMac_address(mac_address);
		sensorToInstall.setX_pos(mouse.x);
		sensorToInstall.setY_pos(mouse.y);
		try {
			Socket sck = SocketClientRequestor.instance().getConnection();
			DataInputStream is = new DataInputStream(sck.getInputStream());
	         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
	         
			 Request request = new Request();
			 request.setRequest("install-sensor");
			 request.setObject(sensorToInstall);
			 String jsonRequest = SerializationDeserialization.serialization(request);
			 os.writeUTF(jsonRequest);
			 
			 String jsonResponse = is.readUTF();
			 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse);
			 
			 String message = PB_INSTALLATION;
			 if (finalResponse.getSuccess() && finalResponse.getRequest().equalsIgnoreCase(INSTALL_END_RESPONSE)) {
				 System.out.println(SENSOR_INSTALLED);
				 message = SENSOR_INSTALLED;
				 isf.dispose();
			 } else {
				 HashMap<String, String> errorMessages = new HashMap<String, String>();
				 errorMessages = SerializationDeserialization.deserialization(finalResponse.getObject(), errorMessages);
				 message = PB_INSTALLATION;
				 if (errorMessages!=null) {
					 errorMessages.remove("validation");
					 for(String key : errorMessages.keySet()) {
						 message = message +"<br>"+ errorMessages.get(key);
					 }
				 } else {
					 message = message +"<br>Problème serveur";
				}
				 message = message +"</html>";
			 }
			 JOptionPane.showMessageDialog(isf, message, TITLE_INSTALLATION, JOptionPane.INFORMATION_MESSAGE);
	      } catch (UnknownHostException exc) {
	         exc.printStackTrace();
	      } catch (IOException exc) {
	         exc.printStackTrace();
	      }
	}
}
