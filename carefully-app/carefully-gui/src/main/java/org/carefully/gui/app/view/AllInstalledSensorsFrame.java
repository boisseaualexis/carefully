package org.carefully.gui.app.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.carefully.gui.model.table.SensorsInfoModel;
import org.carefully.gui.model.table.StateCellRenderer;
import org.carefully.gui.model.table.TypeCellRenderer;

public class AllInstalledSensorsFrame extends JFrame {
	
	private static final long serialVersionUID = -3822386238002510799L;
	
	private SensorsInfoModel siModel;
	private JTable siTable;
	
	public AllInstalledSensorsFrame() throws HeadlessException {
		super("Capteurs installés");
		
		this.setSize(600, 600);
		
		siModel = new SensorsInfoModel();
		siTable = new JTable(siModel);
		siTable.setAutoCreateRowSorter(true);
		siTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		siTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		siTable.getColumnModel().getColumn(1).setCellRenderer(new TypeCellRenderer());
		siTable.getColumnModel().getColumn(0).setCellRenderer(new StateCellRenderer());
		siTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		siTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		JScrollPane scrollPane = new JScrollPane(siTable);
		scrollPane.setPreferredSize(new Dimension(1000,200));
		this.add(scrollPane);
		
		this.setBackground(Color.GRAY);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

}
