package org.carefully.gui.app;

import org.carefully.gui.app.view.MainFrame;
import org.carefully.gui.service.ClientRefreshSensorData;

public class AppClient {
	
	public static MainFrame mf= new MainFrame();
	
    public static void main(String[] args){
    	
    	Thread refreshSensorDataThread = new Thread(new ClientRefreshSensorData());
        refreshSensorDataThread.start();
    }
}
