package org.carefully.gui.app.view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.carefully.gui.app.controller.TypeStatePair;

public class MapPanel extends JPanel {
	
	private static final long serialVersionUID = 30910119887774755L;
	
	private Image map;
	private Image sensorIcon;
	//List<Point> sensorCenters = new ArrayList<Point>();
	private HashMap<Point, TypeStatePair> sensorCenters = new HashMap<Point, TypeStatePair>();
	
	private static final String MAP_IMAGE = "carefullymap.PNG";
	private static final String SENSOR_IMAGE_EXT = ".png";
	private static final String IMAGE_DIRECTORY = "images/";
	
	public void setSensorCenters(HashMap<Point, TypeStatePair> hashMap) {
		this.sensorCenters = hashMap;
	}

	private static final Rectangle poly1 = new Rectangle(25, 25, 283, 283);
	private static final Rectangle poly2 = new Rectangle(310, 25, 170, 283);
	private static final Rectangle poly3 = new Rectangle(601, 25, 335, 400);
	private static final Rectangle poly4 = new Rectangle(25, 310, 575, 116);
	private static final Rectangle poly5 = new Rectangle(482, 25, 118, 285);
	private static final Rectangle poly6 = new Rectangle(25, 429, 455, 170);
	

	public static Rectangle getPoly1() {
		return poly1;
	}


	public static Rectangle getPoly2() {
		return poly2;
	}


	public static Rectangle getPoly3() {
		return poly3;
	}


	public static Rectangle getPoly4() {
		return poly4;
	}


	public static Rectangle getPoly5() {
		return poly5;
	}


	public static Rectangle getPoly6() {
		return poly6;
	}


	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		// Map
	   try {
		   map = new ImageIcon(getClass().getClassLoader().getResource(IMAGE_DIRECTORY + MAP_IMAGE)).getImage();
		   g.drawImage(map, 10, 10, this);
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	   
	   for(Point pt : sensorCenters.keySet()) {
		    g.setColor(sensorCenters.get(pt).getState());
		    g.fillRect(pt.x-15, pt.y-15, 30, 30);
		   setSensorIcon(new ImageIcon(getClass().getClassLoader().getResource(IMAGE_DIRECTORY + sensorCenters.get(pt).getType() +SENSOR_IMAGE_EXT)).getImage());
		   g.drawImage(sensorIcon, pt.x-15, pt.y-15, this);
		   //g.fillOval(pt.x, pt.y, 10, 10);
		}
	   
	   	/*for(Circle sensorArea : sensorAreas.keySet()) {
		g.drawOval(sensorArea.getxPosition(), sensorArea.getyPosition(), sensorArea.getRadius(), sensorArea.getRadius());
		}*/
	  
		/*g.drawRect(poly1.x, poly1.y, poly1.width, poly1.height);
		g.drawRect(poly2.x, poly2.y, poly2.width, poly2.height);
		g.drawRect(poly3.x, poly3.y, poly3.width, poly3.height);
		g.drawRect(poly4.x, poly4.y, poly4.width, poly4.height);
		g.drawRect(poly5.x, poly5.y, poly5.width, poly5.height);
		g.drawRect(poly6.x, poly6.y, poly6.width, poly6.height);*/
	}


	/*public Image getSensorIcon() {
		return sensorIcon;
	}*/


	public void setSensorIcon(Image sensorIcon) {
		this.sensorIcon = sensorIcon;
	}
}