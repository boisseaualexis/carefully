package org.carefully.gui.app.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.carefully.gui.model.table.SensorsInstallModel;
import org.carefully.gui.model.table.StateCellRenderer;
import org.carefully.gui.model.table.TypeCellRenderer;

public class AllNotInstalledSensorsFrame extends JFrame {

	private static final long serialVersionUID = -1635884570421052432L;
	
	private SensorsInstallModel siModel;
	private JTable siTable;
	
	public AllNotInstalledSensorsFrame() throws HeadlessException {
		super("Capteurs non installés");
		
		this.setSize(600, 600);
		
		siModel = new SensorsInstallModel();
		siTable = new JTable(siModel);
		siTable.setAutoCreateRowSorter(true);
		siTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		siTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		siTable.getColumnModel().getColumn(2).setCellRenderer(new TypeCellRenderer());
		siTable.getColumnModel().getColumn(0).setCellRenderer(new StateCellRenderer());
		siTable.getColumnModel().getColumn(1).setPreferredWidth(250);
		JScrollPane scrollPane = new JScrollPane(siTable);
		scrollPane.setPreferredSize(new Dimension(1000,200));
		this.add(scrollPane);
		
		this.setBackground(Color.GRAY);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

}
