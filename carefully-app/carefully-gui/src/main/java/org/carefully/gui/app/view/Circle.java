package org.carefully.gui.app.view;

import java.awt.Color;
import java.awt.Point;

public class Circle {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((xPosition == null) ? 0 : xPosition.hashCode());
		result = prime * result + ((yPosition == null) ? 0 : yPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circle other = (Circle) obj;
		if (xPosition == null) {
			if (other.xPosition != null)
				return false;
		} else if (!xPosition.equals(other.xPosition))
			return false;
		if (yPosition == null) {
			if (other.yPosition != null)
				return false;
		} else if (!yPosition.equals(other.yPosition))
			return false;
		return true;
	}

	private Integer xPosition;
	private Integer yPosition;
	private Integer radius;
	private Color color;
	
	public Circle(Point point, Integer radius, Color color) {
		super();
		this.xPosition = point.x;
		this.yPosition = point.y;
		this.radius = radius;
		this.color = color;
	}

	public Circle(Point point, Integer radius) {
		super();
		this.xPosition = point.x;
		this.yPosition = point.y;
		this.radius = radius;
	}
	
	public Circle(Integer xPosition, Integer yPosition, Integer radius, Color color) {
		super();
		this.xPosition = xPosition;
		this.yPosition = yPosition;
		this.radius = radius;
		this.color = color;
	}

	public Circle(Integer xPosition, Integer yPosition, Integer radius) {
		super();
		this.xPosition = xPosition;
		this.yPosition = yPosition;
		this.radius = radius;
	}

	public Integer getxPosition() {
		return xPosition;
	}

	public void setxPosition(Integer xPosition) {
		this.xPosition = xPosition;
	}

	public Integer getyPosition() {
		return yPosition;
	}

	public void setyPosition(Integer yPosition) {
		this.yPosition = yPosition;
	}

	public Integer getRadius() {
		return radius;
	}

	public void setRadius(Integer radius) {
		this.radius = radius;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean contains(Point point) {
		int borderTop = xPosition+radius;
		int borderDown = xPosition-radius;
		int borderRight = yPosition+radius;
		int borderLeft = yPosition-radius;
		return ((point.x<borderTop) && (point.x>borderDown) && (point.y<borderRight) && (point.y>borderLeft));
	}


}
