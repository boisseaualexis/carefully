package org.carefully.gui.model.combobox;

import java.util.ArrayList;
import java.util.List;

import org.carefully.commons.bean.SensorType;
import org.carefully.gui.service.ClientSensorTypeService;

public class SensorTypeStringModel extends AbstractStringModel<SensorType> {

	private static final long serialVersionUID = 8373955270447966691L;

	private static final String INSTRUCTION = "Choisissez le type de capteur voulu";
	public static String getInstruction() {
		return INSTRUCTION;
	}

	private ClientSensorTypeService sensorTypeService;
	
	@Override
	public void setStrings() {
		ArrayList<String> listStrings = new ArrayList<String>();
		listStrings.add(INSTRUCTION);
		sensorTypeService = ClientSensorTypeService.getInstance();
		List<SensorType> listSensorTypes = sensorTypeService.findLastSensorTypeInfo();
		for(SensorType sensorType : listSensorTypes) {
			listStrings.add(sensorType.getName_sensor_type());
		}
		this.strings = listStrings;
	}
}
