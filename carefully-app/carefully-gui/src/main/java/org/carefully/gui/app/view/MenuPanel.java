package org.carefully.gui.app.view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MenuPanel extends JPanel {
	
	
	private static final long serialVersionUID = -8347789312272665785L;
	
	private JButton quit, showNotInstalledSensors, showInstalledSensors;
	
	public MenuPanel() {
		super();
		quit = new JButton("Fermer l'application");
		showNotInstalledSensors = new JButton("Gérer les capteurs en stock");
		showInstalledSensors = new JButton("Gérer les capteurs installés");

		this.setLayout(new GridLayout(6,1));
		this.add(quit);
		this.add(showNotInstalledSensors);
		this.add(showInstalledSensors);
		this.setBackground(Color.GRAY);
		
	}

	public JButton getQuit() {
		return quit;
	}

	public JButton getShowNotInstalledSensors() {
		return showNotInstalledSensors;
	}

	public JButton getShowInstalledSensors() {
		return showInstalledSensors;
	}
}
