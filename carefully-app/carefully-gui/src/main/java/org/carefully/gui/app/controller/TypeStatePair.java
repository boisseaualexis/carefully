package org.carefully.gui.app.controller;

import java.awt.Color;

public class TypeStatePair {
	
	private String type;
	private Color state;
	
	protected TypeStatePair(String type, String state) {
		super();
		this.type = type;
		this.state = findColor(state);
	}

	private Color findColor(String state) {
		Color color = Color.LIGHT_GRAY;
		switch (state) {
		case "normal":
			color = (Color.GREEN);
			break;
		case "attention":
			color = (Color.ORANGE);
			break;
		case "alerte":
			color = (Color.RED);
			break;
		case "panne":
			color = (Color.MAGENTA);
			break;
		case "non installe":
			color = (Color.GREEN);
			break;
		case "desinstalle":
			color = (Color.GRAY);
			break;

		default:
			color = (Color.LIGHT_GRAY);
			break;
		}
		return color;
	}

	public String getType() {
		return type;
	}

	protected void setType(String type) {
		this.type = type;
	}

	public Color getState() {
		return state;
	}

	protected void setState(Color state) {
		this.state = state;
	}
	

}
