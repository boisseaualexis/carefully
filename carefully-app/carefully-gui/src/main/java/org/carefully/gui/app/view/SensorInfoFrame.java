package org.carefully.gui.app.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.carefully.commons.bean.Sensor;
import org.carefully.gui.model.table.SensorsInfoModel;
import org.carefully.gui.model.table.StateCellRenderer;
import org.carefully.gui.model.table.TypeCellRenderer;

public class SensorInfoFrame extends JFrame {

	private static final long serialVersionUID = 7023426847704114910L;

	private JButton uninstallSensor, exit;
	private SensorsInfoModel siModel;
	private JTable siTable;
	private JPanel bottomPanel;
	private JLabel lastUpdateLabel;

	public SensorInfoFrame(Sensor sensor) {
		super("Capteur");
		uninstallSensor = new JButton("Désinstaller ce capteur");
		exit = new JButton("Fermer");
		bottomPanel = new JPanel(new BorderLayout(5, 5));
		lastUpdateLabel = new JLabel();

		// this.setLocation(150,150);
		this.setSize(600, 200);
		this.setLayout(new BorderLayout(5, 5));

		siModel = new SensorsInfoModel(sensor);
		siTable = new JTable(siModel);
		siTable.setAutoCreateRowSorter(true);
		siTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		siTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		siTable.getColumnModel().getColumn(1).setCellRenderer(new TypeCellRenderer());
		siTable.getColumnModel().getColumn(0).setCellRenderer(new StateCellRenderer());
		siTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		siTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		JScrollPane scrollPane = new JScrollPane(siTable);
		scrollPane.setPreferredSize(new Dimension(1000, 100));
		this.add(scrollPane, BorderLayout.CENTER);

		bottomPanel.add(uninstallSensor, BorderLayout.WEST);
		bottomPanel.add(exit, BorderLayout.EAST);
		bottomPanel.add(lastUpdateLabel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);
		this.setBackground(Color.GRAY);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getUninstallSensor() {
		return uninstallSensor;
	}

	public SensorsInfoModel getSiModel() {
		return siModel;
	}

	public JTable getSiTable() {
		return siTable;
	}

	public JLabel getLastUpdate() {
		return lastUpdateLabel;
	}

	public void setLastUpdate(JLabel lastUpdate) {
		this.lastUpdateLabel = lastUpdate;
	}

}
