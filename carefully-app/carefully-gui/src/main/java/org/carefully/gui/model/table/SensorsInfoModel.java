package org.carefully.gui.model.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.carefully.commons.bean.Sensor;
import org.carefully.gui.service.ClientSensorService;

public class SensorsInfoModel extends AbstractTableModel {

	private static final long serialVersionUID = 7697721892021437910L;

	private final String[] headers = { "Etat", "Type", "Valeur", "Adresse MAC" };

	private ClientSensorService sensorService;

	private List<Sensor> sensors;

	private List<Sensor> listAllSensors;

	public SensorsInfoModel(String room) {
		initialize(room);
	}

	public void initialize(String room) {
		sensorService = ClientSensorService.getInstance();
		listAllSensors = sensorService.findLastSensorInfo();
		sensors = filterByRoom(listAllSensors, room);
		fireTableDataChanged();
	}
	
	public SensorsInfoModel(Sensor sensor) {
		initialize(sensor);
	}

	public void initialize(Sensor sensor) {
		sensorService = ClientSensorService.getInstance();
		listAllSensors = sensorService.findLastSensorInfo();
		sensors = findByMacAddress(listAllSensors, sensor);
		fireTableDataChanged();
	}

	private List<Sensor> findByMacAddress(List<Sensor> listAllSensors, Sensor sensor) {
		List<Sensor> result = new ArrayList<Sensor>();
		String macAddress = sensor.getMac_address();
		for (Sensor sensorTest : listAllSensors) {
			if (sensorTest.getMac_address().equalsIgnoreCase(macAddress)) {

				result.add(sensor);
				break;
			}
		}
		return result;
	}

	public SensorsInfoModel() {
		initialize();
	}

	public void initialize() {
		sensorService = ClientSensorService.getInstance();
		listAllSensors = sensorService.findLastSensorInfo();
		sensors = listAllSensors;
		sensors = filterByStateInstalled(listAllSensors);
		fireTableDataChanged();
	}

	private List<Sensor> filterByStateInstalled(List<Sensor> listAllSensors) {
		List<Sensor> result = new ArrayList<Sensor>();
		for (Sensor sensor : listAllSensors) {
			if (!((sensor.getFk_state().equalsIgnoreCase("non installe")) || (sensor.getFk_state().equalsIgnoreCase("desinstalle")))) { // Installed sensors

				result.add(sensor);
			}
		}
		return result;
	}

	private List<Sensor> filterByRoom(List<Sensor> listAllSensors, String room) {
		List<Sensor> result = new ArrayList<Sensor>();
		for (Sensor sensor : listAllSensors) {
			if (sensor.getFk_room().equalsIgnoreCase(room)) {

				result.add(sensor);
			}
		}
		return result;
	}

	/*
	 * public List<Sensor> getSensors(){ return sensors; }
	 */

	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return headers[columnIndex];
	}

	@Override
	public int getRowCount() {
		return sensors.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {

		case 0:
			// State
			return sensors.get(rowIndex).getFk_state();

		case 1:
			// Type
			return sensors.get(rowIndex).getFk_type();

		case 2:
			// Value
			return sensors.get(rowIndex).getValue();

		case 3:
			// Mac address
			return sensors.get(rowIndex).getMac_address();

		default:
			throw new IllegalArgumentException();
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
		case 1:
		case 3:
			return String.class;
		case 2:
			return Integer.class;

		default:
			return Object.class;
		}
	}

	public void deleteRowAt(int rowIndex) {
		sensors.remove(rowIndex);
		fireTableDataChanged();
	}

}
