package org.carefully.gui.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.carefully.commons.bean.Room;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.gui.config.SocketClientRequestor;

public class ClientRoomService {
	
	private static ClientRoomService instance;
	
	private List<Room> rooms;

	private ClientRoomService() {
		selectAllRooms();
	}
	
	private void selectAllRooms() {
		List<Room> listRooms = new ArrayList<Room>();
		try {
			Socket sck = SocketClientRequestor.instance().getConnection();
			DataInputStream is = new DataInputStream(sck.getInputStream());
	         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
	         
			 Request request = new Request();
			 request.setRequest("select-room-all");
			 String jsonRequest = SerializationDeserialization.serialization(request);
			 os.writeUTF(jsonRequest);
			 
			 String jsonResponse = is.readUTF();
			 Request response = SerializationDeserialization.deserialization(jsonResponse);
			 Room room = new Room();
			 
			 while (response.getRequest().startsWith("room-next")) {
				 room = SerializationDeserialization.deserialization(response.getObject(), room);
				 listRooms.add(room);
				 jsonResponse = is.readUTF();
				 response = SerializationDeserialization.deserialization(jsonResponse);
			 }
			 
			 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse);
			 if (finalResponse.getSuccess()) {
				 this.rooms = listRooms;
				 System.out.println("Toutes les pièces communes sont récupérées.");
			 } else {
				 System.out.println("Problème, la liste des pièces communes n'est pas disponible.");
			 }
	      } catch (UnknownHostException exc) {
	         exc.printStackTrace();
	      } catch (IOException exc) {
	         exc.printStackTrace();
	      }
		
	}

	public static synchronized ClientRoomService getInstance() {
		if (instance == null) {
			instance = new ClientRoomService();
		}
		return instance;
	}

	public synchronized List<Room> findLastRoomInfo() {
		//selectAllSensorTypes();
		return rooms;
	}
}
