package org.carefully.gui.app.view;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import org.carefully.gui.app.controller.ControllerMapPanel;
import org.carefully.gui.app.controller.ControllerMenuPanel;

public class MainPanel extends JPanel {
	
	private static final long serialVersionUID = 3175090880102987165L;
	
	private MenuPanel menuP;
	private MapPanel mapP;
	private ControllerMapPanel controlMapP;
	private ControllerMenuPanel controlMenuP;
	
	private BorderLayout bl;
	
	public MainPanel() {
		super();
		menuP = new MenuPanel();
		controlMenuP = new ControllerMenuPanel(menuP);
		mapP = new MapPanel();
		controlMapP = new ControllerMapPanel(mapP);
		bl = new BorderLayout();
		this.setLayout(bl);
		this.add(menuP,BorderLayout.WEST);
		this.add(mapP,BorderLayout.CENTER);
	}

}
