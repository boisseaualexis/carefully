package org.carefully.gui.app.view;

import java.awt.CardLayout;
import javax.swing.JFrame;

public class MainFrame extends JFrame {
	
		private static final long serialVersionUID = 4279328439747866650L;
		
		private CardLayout cd;
		private MainPanel mp;
		
		public MainFrame() {
			super("CareFully");
			cd = new CardLayout();
			this.setLayout(cd);
			mp = new MainPanel();
			
			// Add panel to the main frame
			this.getContentPane().add("main",mp);
			
			cd.show(this.getContentPane(), "main");
			
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setLocation(50,50);
			this.setSize(1180, 680);
			this.setVisible(true);
		}
		
		public CardLayout getCd() {
			return cd;
		}
		public MainPanel getMp() {
			return mp;
		}
}
