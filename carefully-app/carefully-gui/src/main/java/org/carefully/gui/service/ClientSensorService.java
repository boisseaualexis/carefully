package org.carefully.gui.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.carefully.commons.bean.Sensor;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.gui.config.SocketClientRequestor;

public class ClientSensorService {
	
	private static ClientSensorService instance;
	
	private List<Sensor> sensors;
	private String lastUpdate = null;

	public String getLastUpdate() {
		return lastUpdate;
	}

	private ClientSensorService() {
		selectAllSensors();
	}
	
	public synchronized void RefreshSensorInfo() {
		selectAllSensors();
	}
	
	private void selectAllSensors() {
		List<Sensor> listSensors = new ArrayList<Sensor>();
		try {
			Socket sck = SocketClientRequestor.instance().getConnection();
			DataInputStream is = new DataInputStream(sck.getInputStream());
	         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
	         
			 Request request = new Request();
			 request.setRequest("select-sensor-all");
			 String jsonRequest = SerializationDeserialization.serialization(request);
			 os.writeUTF(jsonRequest);
			 
			 String jsonResponse = is.readUTF();
			 Request response = SerializationDeserialization.deserialization(jsonResponse);
			 Sensor sensor = new Sensor();
			 
			 while (response.getRequest().startsWith("sensor-next")) {
				 sensor = SerializationDeserialization.deserialization(response.getObject(), sensor);
				 listSensors.add(sensor);
				 jsonResponse = is.readUTF();
				 response = SerializationDeserialization.deserialization(jsonResponse);
			 }
			 
			 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse);
			 if (finalResponse.getSuccess()) {
				 this.sensors = listSensors;
				 DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				 Date date = new Date();
				 lastUpdate = format.format(date);
				 System.out.println("All sensors collected from the server");
			 } else {
				 System.out.println("Error : sensor list not available from the server");
			 }
	      } catch (UnknownHostException exc) {
	         exc.printStackTrace();
	      } catch (IOException exc) {
	         exc.printStackTrace();
	      }
		
	}

	public static synchronized ClientSensorService getInstance() {
		if (instance == null) {
			instance = new ClientSensorService();
		}
		return instance;
	}

	public synchronized List<Sensor> findLastSensorInfo() {
		//selectAllSensors();
		return sensors;
	}
}
