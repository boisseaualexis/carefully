package org.carefully.gui.app.controller;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;

import org.carefully.commons.bean.Sensor;
import org.carefully.gui.app.view.Circle;
import org.carefully.gui.app.view.MapPanel;
import org.carefully.gui.app.view.RoomInfoFrame;
import org.carefully.gui.app.view.SensorInfoFrame;
import org.carefully.gui.service.ClientSensorService;

public class ControllerMapPanel implements MouseListener, ActionListener {
	
	private MapPanel mapPanel;
	
	private static final String NOT_INSTALLED = "non installe";
	private static final String UNINSTALLED = "desinstalle";

	private static final Integer SENSOR_AREA_RADIUS = 20;
	
	private List<Sensor> sensors;
	//private List<Point> centers;
	private HashMap<Point, TypeStatePair> centers;
	private HashMap<Circle, Sensor> sensorAreas;

	public ControllerMapPanel(MapPanel mapPanel) {
		super();
		sensors = ClientSensorService.getInstance().findLastSensorInfo();
		this.mapPanel = mapPanel;
		this.mapPanel.addMouseListener(this);
		this.mapPanel.setSensorCenters(findSensorCenters());
		findSensorAreas();
		Thread refreshMapThread = new Thread(new RefreshMap(this));
        refreshMapThread.start();
	}
	
	private HashMap<Point, TypeStatePair> findSensorCenters() {
		TypeStatePair tsPair = null;
		HashMap<Point, TypeStatePair> listCenters = new HashMap<Point, TypeStatePair>();
		for (Sensor sensor : sensors) {
			if(!(sensor.getFk_state().equalsIgnoreCase(NOT_INSTALLED) || sensor.getFk_state().equalsIgnoreCase(UNINSTALLED))) {
				Point center = new Point(sensor.getX_pos(), sensor.getY_pos());
				tsPair = new TypeStatePair(sensor.getFk_type(), sensor.getFk_state());
				listCenters.put(center, tsPair);
			}
		}
		centers = listCenters;
		return centers;
	}
	
	/*private List<Point> findSensorCenters() {
		List<Point> listCenters = new ArrayList<Point>();
		for (Sensor sensor : sensors) {
			if(!(sensor.getFk_state().equalsIgnoreCase(NOT_INSTALLED) || sensor.getFk_state().equalsIgnoreCase(UNINSTALLED))) {
				Point center = new Point(sensor.getX_pos(), sensor.getY_pos());
				listCenters.add(center);
			}
		}
		centers = listCenters;
		return centers;
	}*/
	
	private HashMap<Circle, Sensor> findSensorAreas() {
		HashMap<Circle, Sensor> areas = new HashMap<Circle, Sensor>();
		for (Sensor sensor : sensors) {
			if(!(sensor.getFk_state().equalsIgnoreCase(NOT_INSTALLED) || sensor.getFk_state().equalsIgnoreCase(UNINSTALLED))) {
				Circle area = new Circle(sensor.getX_pos(), sensor.getY_pos(), SENSOR_AREA_RADIUS);
				areas.put(area, sensor);
			}
		}
		sensorAreas = areas;
		return sensorAreas;
	}
	
	protected void repaintMap() {
		sensors = ClientSensorService.getInstance().findLastSensorInfo();
		this.mapPanel.setSensorCenters(findSensorCenters());
		findSensorAreas();
		this.mapPanel.repaint();;
	}

	private boolean location(Point mouse, Rectangle area) {
		if (area.contains(mouse))
			return true;
		else
			return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point mouse = e.getPoint();
		String room = null;
		Sensor sensor = null;
		
		this.mapPanel.getGraphics().drawOval(mouse.x, mouse.y, 7, 7);
				
		room = roomSelectedByMouse(mouse);
		sensor = sensorSelectedByMouse(mouse);
		if(!(sensor==null)) {
			SensorInfoFrame sif = new SensorInfoFrame(sensor);
			ControllerSensorInfoFrame controllerRif = new ControllerSensorInfoFrame(sensor, sif);
		}
		else if(!(room==null)) {
			RoomInfoFrame rif = new RoomInfoFrame(room);
			ControllerRoomInfoFrame controllerRif = new ControllerRoomInfoFrame(room, mouse, rif);
		}
	}

	private Sensor sensorSelectedByMouse(Point mouse) {
		Sensor sensor = null;
		Circle area = circleSelectedByMouse(mouse);
		if (area==null) {
			return null;
		}
		sensor = sensorAreas.get(area);
		return sensor;
	}

	private Circle circleSelectedByMouse(Point mouse) {
		Circle circle = null;
		
		for(Circle sensorArea : sensorAreas.keySet()) {
			if(sensorArea.contains(mouse)) {
				circle = sensorArea;
				break;
			}
		}
		return circle;
	}

	private String roomSelectedByMouse(Point mouse) {
		String room = null;
		
		if (location(mouse, mapPanel.getPoly1())) {
			
			room = "Kitchen";
		}
		else if (location(mouse, mapPanel.getPoly2())) {

			room = "Bathroom";
		}
		else if (location(mouse, mapPanel.getPoly3())) {

			room = "Common room";
		}
		else if (location(mouse, mapPanel.getPoly4()) || location(mouse, mapPanel.getPoly5())) {

			room = "Hallway";
		}
		else if (location(mouse, mapPanel.getPoly6())) {

			room = "Reception";
		}
		return room;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
