package org.carefully.gui.model.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.carefully.commons.bean.Sensor;
import org.carefully.gui.service.ClientSensorService;

public class SensorsInstallModel extends AbstractTableModel {

	private static final long serialVersionUID = 7697721892021437910L;

	private static final String NOT_INSTALLED = "non installe";

	private static final String UNINSTALLED = "desinstalle";
	
	private static final String ALL_AVAILABLE_INSTRUCTION = "Choisissez le type de capteur voulu";
	
	private final String[] headers = { "Info", "Capteurs disponibles en stock", "Type" };

	private ClientSensorService sensorService;

	private List<Sensor> sensors;

	private List<Sensor> listAllSensors;
	
	public SensorsInstallModel(String type) {
		initialize(type);
	}

	public void initialize(String type) {
		sensorService = ClientSensorService.getInstance();
		listAllSensors = sensorService.findLastSensorInfo();
		sensors = filterByTypeAndAvailability(listAllSensors, type);
		fireTableDataChanged();
	}
	
	public SensorsInstallModel() {
		initialize();
	}
	
	public void initialize() {
		sensorService = ClientSensorService.getInstance();
		listAllSensors = sensorService.findLastSensorInfo();
		sensors = filterByAvailability(listAllSensors);
		fireTableDataChanged();
	}
	
	private List<Sensor> filterByAvailability(List<Sensor> listAllSensors) {
		List<Sensor> result = new ArrayList<Sensor>();
		for (Sensor sensor : listAllSensors) {
			if ((sensor.getFk_state().equalsIgnoreCase("non installe")) || (sensor.getFk_state().equalsIgnoreCase("desinstalle"))) { // Not nstalled sensors

				result.add(sensor);
			}
		}
		return result;
	}
	
	private List<Sensor> filterByTypeAndAvailability(List<Sensor> listAllSensors, String type) {
		List<Sensor> result = new ArrayList<Sensor>();
		if (type.equalsIgnoreCase(ALL_AVAILABLE_INSTRUCTION)) {
			for (Sensor sensor: listAllSensors) {
			    if (sensor.getFk_state().equalsIgnoreCase(NOT_INSTALLED) || sensor.getFk_state().equalsIgnoreCase(UNINSTALLED)) {

					result.add(sensor);
			    }
			}
		}
		else {
			for (Sensor sensor: listAllSensors) {
			    if (sensor.getFk_type().equalsIgnoreCase(type) && (sensor.getFk_state().equalsIgnoreCase(NOT_INSTALLED) || sensor.getFk_state().equalsIgnoreCase(UNINSTALLED))) {
	
					result.add(sensor);
			    }
			}
		}
		return result;		
	}

	/*public List<Sensor> getSensors(){
		return sensors;
	}*/
	
	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return headers[columnIndex];
	}

	@Override
	public int getRowCount() {
		return sensors.size();
	}

	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		
		case 0:
			//N°
			return sensors.get(rowIndex).getFk_state();
		case 1:
			//Available sensors
			return sensors.get(rowIndex).getMac_address();
		case 2:
			//Available sensors
			return sensors.get(rowIndex).getFk_type();

		default:
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
			case 1:
			case 2:
				return String.class;

			default:
				return Object.class;
		}
	}

}
