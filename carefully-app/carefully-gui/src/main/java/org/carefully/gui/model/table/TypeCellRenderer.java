package org.carefully.gui.model.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TypeCellRenderer extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = -5380999827390662760L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,	row, column);

		String type = (String) value;
		setText(type.toString());
		
		setBackground(Color.LIGHT_GRAY);
		
		switch (type) {
		case "fumee":
			setForeground(Color.DARK_GRAY);
			break;
		case "temperature":
			setForeground(Color.RED);
			break;
		case "humidite":
			setForeground(Color.BLUE);
			break;
		case "luminosite":
			setForeground(Color.YELLOW);
			break;
		case "co":
			setForeground(Color.BLACK);
			break;
		case "presence":
			setForeground(Color.ORANGE);
			break;			

		default:
			setForeground(Color.GREEN);
			break;
		}

		return this;
	}
}