package org.carefully.commons.bean;

public class Room {
	
	private String name_room;
	private Integer x_pos_room;
	private Integer y_pos_room;
	private Integer width_room;
	private Integer height_room;
	
	public String getName_room() {
		return name_room;
	}
	public void setName_room(String name_room) {
		this.name_room = name_room;
	}
	public Integer getX_pos_room() {
		return x_pos_room;
	}
	public void setX_pos_room(Integer x_pos_room) {
		this.x_pos_room = x_pos_room;
	}
	public Integer getY_pos_room() {
		return y_pos_room;
	}
	public void setY_pos_room(Integer y_pos_room) {
		this.y_pos_room = y_pos_room;
	}
	public Integer getWidth_room() {
		return width_room;
	}
	public void setWidth_room(Integer width_room) {
		this.width_room = width_room;
	}
	public Integer getHeight_room() {
		return height_room;
	}
	public void setHeight_room(Integer height_room) {
		this.height_room = height_room;
	}

}
