package org.carefully.commons.bean;

public class SensorType {
	
	private String name_sensor_type;
	private Integer range_sensor_type;
	
	public String getName_sensor_type() {
		return name_sensor_type;
	}
	public void setName_sensor_type(String name_sensor_type) {
		this.name_sensor_type = name_sensor_type;
	}
	public Integer getRange_sensor_type() {
		return range_sensor_type;
	}
	public void setRange_sensor_type(Integer range_sensor_type) {
		this.range_sensor_type = range_sensor_type;
	}

}
