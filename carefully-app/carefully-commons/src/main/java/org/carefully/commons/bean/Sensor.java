package org.carefully.commons.bean;

public class Sensor implements Cloneable {
	
	@Override
	public Sensor clone() throws CloneNotSupportedException {
		return (Sensor) super.clone();
	}
	private String ip_address;
	private String mac_address;
	private String fk_state;
	private String fk_type;
	private String fk_room;
	private Integer x_pos;
	private Integer y_pos;
	private Integer value;
	
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getMac_address() {
		return mac_address;
	}
	public void setMac_address(String mac_address) {
		this.mac_address = mac_address;
	}
	public String getFk_state() {
		return fk_state;
	}
	public void setFk_state(String fk_state) {
		this.fk_state = fk_state;
	}
	public String getFk_type() {
		return fk_type;
	}
	public void setFk_type(String fk_type) {
		this.fk_type = fk_type;
	}
	public String getFk_room() {
		return fk_room;
	}
	public void setFk_room(String fk_room) {
		this.fk_room = fk_room;
	}
	public Integer getX_pos() {
		return x_pos;
	}
	public void setX_pos(Integer x_pos) {
		this.x_pos = x_pos;
	}
	public Integer getY_pos() {
		return y_pos;
	}
	public void setY_pos(Integer y_pos) {
		this.y_pos = y_pos;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Sensor [ip_address=" + ip_address + ", mac_address=" + mac_address + ", fk_state=" + fk_state
				+ ", fk_type=" + fk_type + ", fk_room=" + fk_room + ", x_pos=" + x_pos + ", y_pos=" + y_pos + ", value="
				+ value + "]";
	}
}
