package org.carefully.server.deamon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DemoThread extends ScenarioThread {

	public DemoThread(Connection cn) throws IOException {
		super(cn);
	}

	@Override
	public void run() {
		try {
			boolean result = this.cnDB.createStatement().execute("truncate table sensor;"
+"INSERT INTO public.sensor VALUES (60, NULL, '5b:8a:4b:0f:55:53        ', NULL, NULL, NULL, 0, 1, 1);"
+"INSERT INTO public.sensor VALUES (61, NULL, 'e9:06:9c:27:d3:48        ', NULL, NULL, NULL, 0, 1, 3);"
+"INSERT INTO public.sensor VALUES (62, NULL, '08:50:33:8c:a1:83        ', NULL, NULL, NULL, 0, 1, 2);"
+"INSERT INTO public.sensor VALUES (64, NULL, 'b7:33:81:e6:1e:9d        ', NULL, NULL, NULL, 0, 1, 6);"
+"INSERT INTO public.sensor VALUES (65, NULL, 'd7:db:6a:c0:48:0d        ', NULL, NULL, NULL, 0, 1, 6);"
+"INSERT INTO public.sensor VALUES (49, '10.10.10.22              ', 'ad:d9:0c:35:47:41        ', 288, 400, 20, 2, 2, 3);"
+"INSERT INTO public.sensor VALUES (40, '10.10.10.21              ', '49:fa:08:ec:2e:e6        ', 150, 360, 10, 2, 2, 1);"
+"INSERT INTO public.sensor VALUES (63, NULL, 'e1:ab:26:03:8f:10        ', NULL, NULL, NULL, 0, 1, 4);"
+"INSERT INTO public.sensor VALUES (41, NULL, '99:99:e8:70:ec:28        ', NULL, NULL, NULL, 0, 6, 1);"
+"INSERT INTO public.sensor VALUES (38, '10.10.10.19              ', '30:d2:05:42:59:0d        ', 500, 220, 1, 3, 2, 5);"
+"INSERT INTO public.sensor VALUES (67, NULL, 'd2:46:84:53:de:6f        ', NULL, NULL, NULL, 0, 6, 1);"
+"INSERT INTO public.sensor VALUES (56, '10.10.10.35              ', '97:b7:7c:9c:9c:fb        ', 500, 130, 25, 3, 2, 6);"
+"INSERT INTO public.sensor VALUES (53, '10.10.10.32              ', 'fa:0a:4b:e0:b1:1d        ', 535, 110, 40, 3, 2, 4);"
+"INSERT INTO public.sensor VALUES (69, NULL, '9a:07:c0:a2:4d:9e        ', NULL, NULL, NULL, 0, 6, 5);"
+"INSERT INTO public.sensor VALUES (48, NULL, '90:0f:9c:75:9a:5c        ', NULL, NULL, NULL, 0, 1, 3);"
+"INSERT INTO public.sensor VALUES (68, NULL, '1c:93:24:43:99:96        ', NULL, NULL, NULL, 0, 6, 3);"
+"INSERT INTO public.sensor VALUES (71, NULL, 'de:76:2f:2d:0b:28        ', NULL, NULL, NULL, 0, 6, 2);"
+"INSERT INTO public.sensor VALUES (70, NULL, '4e:21:d6:75:65:67        ', NULL, NULL, NULL, 0, 6, 5);"
+"INSERT INTO public.sensor VALUES (46, '10.10.10.27              ', '62:da:2b:86:b6:62        ', 35, 435, 8, 4, 2, 2);"
+"INSERT INTO public.sensor VALUES (58, '10.10.10.37              ', '4b:6d:9c:82:c6:58        ', 165, 175, 40, 1, 3, 6);"
+"INSERT INTO public.sensor VALUES (54, '10.10.10.33              ', '4c:5b:1e:e2:8b:84        ', 140, 470, 43, 4, 2, 4);"
+"INSERT INTO public.sensor VALUES (50, '10.10.10.29              ', 'f8:3b:00:f5:71:8b        ', 248, 438, 20, 4, 2, 3);"
+"INSERT INTO public.sensor VALUES (42, '10.10.10.23              ', '34:87:26:16:40:85        ', 325, 500, 9, 4, 2, 1);"
+"INSERT INTO public.sensor VALUES (39, '10.10.10.20              ', 'db:58:f7:c3:6d:0e        ', 320, 170, 1, 5, 2, 5);"
+"INSERT INTO public.sensor VALUES (51, '10.10.10.30              ', '87:90:0e:ba:38:cf        ', 458, 220, NULL, 5, 5, 3);"
+"INSERT INTO public.sensor VALUES (57, '10.10.10.36              ', 'dc:9f:e0:f4:64:74        ', 248, 578, 27, 4, 2, 6);"
+"INSERT INTO public.sensor VALUES (43, '10.10.10.24              ', 'fa:83:a5:8e:dc:bd        ', 390, 110, 15, 5, 2, 1);"
+"INSERT INTO public.sensor VALUES (59, '10.10.10.38              ', '67:53:d3:fe:1d:6f        ', 390, 260, NULL, 5, 5, 6);"
+"INSERT INTO public.sensor VALUES (47, '10.10.10.28              ', '9c:fb:05:88:18:ea        ', 720, 160, 124, 6, 4, 2);"
+"INSERT INTO public.sensor VALUES (44, '10.10.10.25              ', 'df:28:21:3f:fe:d5        ', 740, 260, 55, 6, 3, 1);"
+"INSERT INTO public.sensor VALUES (55, '10.10.10.34              ', 'fe:be:8e:d4:2e:70        ', 770, 300, 50, 6, 2, 4);"
+"INSERT INTO public.sensor VALUES (52, '10.10.10.31              ', '46:b5:23:55:25:64        ', 610, 200, 34, 6, 4, 3);"
+"INSERT INTO public.sensor VALUES (45, '10.10.10.26              ', '36:3d:b1:ad:5c:9e        ', 190, 360, 11, 2, 2, 2);"
+"INSERT INTO public.sensor VALUES (36, '10.10.10.17              ', 'f2:80:3e:6c:0a:c5        ', 200, 290, 0, 1, 2, 5);"
+"INSERT INTO public.sensor VALUES (37, '10.10.10.18              ', '9c:61:a7:4b:66:fa        ', 410, 320, NULL, 2, 5, 5);"
			      );
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
