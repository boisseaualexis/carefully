package org.carefully.server.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectionPoolConfig {

    static private ConnectionPoolConfig _instance = null;
    static private String capacity = null;
    static private String maxCapacity = null;
	 
    private ConnectionPoolConfig(){
    	InputStream file = null;
    	Properties props = new Properties();

	    try{
	        file = getClass().getResourceAsStream("/connectionpool.properties");
	        props.load(file);
	        capacity = props.getProperty("CAPACITY");
	        maxCapacity = props.getProperty("MAXCAPACITY");
	       } 
	    catch(Exception e){
	        System.out.println("error" + e);
	       }
	    finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					file=null;
				}
			}
		}
    }
	
	static public ConnectionPoolConfig instance(){
        if (_instance == null) {
            _instance = new ConnectionPoolConfig();
        }
        return _instance;
    }

	public static String getCapacity() {
		return capacity;
	}

	public static String getMaxCapacity() {
		return maxCapacity;
	}
}