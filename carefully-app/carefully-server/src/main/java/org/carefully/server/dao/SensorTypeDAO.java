package org.carefully.server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.carefully.commons.bean.SensorType;

public class SensorTypeDAO extends DAO<SensorType> {

	public SensorTypeDAO(Connection cn) {
		super(cn);
	}

	@Override
	public boolean create(SensorType obj) throws SQLException {
		// NOT USED
		return false;
	}

	@Override
	public boolean delete(SensorType obj) {
		// NOT USED
		return false;
	}

	@Override
	public boolean update(SensorType obj) {
		// NOT USED
		return false;
	}

	@Override
	public SensorType find(int id) throws SQLException {
		SensorType sensorType = new SensorType();
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		      ).executeQuery(
		        "SELECT * FROM sensor_type WHERE id = "+id
		      );
		result.next();
		sensorType.setName_sensor_type(result.getString("name_sensor_type").trim());
		return sensorType;
	}

	@Override
	public ResultSet findAll() throws SQLException {
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		      ).executeQuery(
		    	"SELECT * FROM sensor_type"
		      );
		return result;
	}

}
