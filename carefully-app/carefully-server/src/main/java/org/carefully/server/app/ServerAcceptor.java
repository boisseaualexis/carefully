package org.carefully.server.app;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import org.carefully.server.config.SocketServerConfig;
import org.carefully.server.db.connection.pool.ConnectionException;
import org.carefully.server.db.connection.pool.ConnectionPool;

public class ServerAcceptor {
	
	//Initialize ServerSocket configuration
	SocketServerConfig socketServerConfig = SocketServerConfig.instance();
	
	//Initialize DB connection pool
	ConnectionPool connectionPool = ConnectionPool.instance(); 
	
	   //Initialize default values
	   private Integer port = 2009;
	   private String host = "localhost";
	   private ServerSocket server = null;
	   private boolean isRunning = true;
	   
	   public ServerAcceptor(){
		  try {
			  port = Integer.valueOf(SocketServerConfig.getPort());
		  } catch (Exception e) {
		      e.printStackTrace();
		      System.out.println("Pb Config");
		  }
	      try {
	         server = new ServerSocket(port);
	         System.out.println("Server on "+host+" listening on port "+port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	   
	   public ServerAcceptor(String newHost, int newPort){
	      host = newHost;
	      port = newPort;
	      try {
	         server = new ServerSocket(port, 100, InetAddress.getByName(host));
	         System.out.println("Server on "+host+" listening on port "+port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	   
	   
	   //Starting server
	   public void open(){
	      
	      //In a thread because it is an infinite loop
	      Thread t = new Thread(new Runnable(){
	         public void run(){
	            while(isRunning == true){
	               Connection cnClient = null;
	               try {
	            	  //Get connection to DB
	            	  cnClient = ConnectionPool.getConnection();

	                  //Waiting client connection
	                  Socket sckClient = server.accept();
	                  
	                  //Process client connection in a other thread
	                  System.out.println("Client connection accepted : "+ sckClient.getRemoteSocketAddress());
	                  Thread t = new Thread(new RequestHandler(sckClient, cnClient));
	                  t.start();
	               } catch (IOException e) {
	            	  ConnectionPool.free(cnClient);
	                  System.out.println("Error : pb socket client");
	                  e.printStackTrace();
	                  
	               } catch (ConnectionException e) {
					System.out.println("Error : " + e.getMessage());
					e.printStackTrace();
	               }
	            }
	            
	            try {
	               server.close();
	            } catch (IOException e) {
	               e.printStackTrace();
	               server = null;
	            }
	         }
	      });
	      
	      t.start();
	   }
	   
	   public void close(){
	      isRunning = false;
	   }   
}
