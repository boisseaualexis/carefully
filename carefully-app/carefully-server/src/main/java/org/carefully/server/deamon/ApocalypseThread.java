package org.carefully.server.deamon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

public class ApocalypseThread extends ScenarioThread {

	Random rdm;

	public ApocalypseThread(Connection cn) throws IOException {
		super(cn);
		rdm = new Random();
	}

	@Override
	public void run() {
		Thread allGoodThread;
		try {
			allGoodThread = new Thread(new AllGoodThread(cnDB));
			allGoodThread.start();
			allGoodThread.join();
		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
		}

		for (int i = 0; i < 155; i++) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			int sensorId = rdm.nextInt(36) + 36;
			int state = rdm.nextInt(2) + 4;
			for (int j = sensorId - 3; j < sensorId + 4; j++) {
				try {
					int result = this.cnDB.createStatement().executeUpdate("UPDATE sensor " + "SET fk_state=" + state
							+ ", " + "value=0 " + "WHERE fk_room>0 and id=" + sensorId);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
