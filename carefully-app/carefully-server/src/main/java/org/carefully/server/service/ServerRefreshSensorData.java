package org.carefully.server.service;

public class ServerRefreshSensorData implements Runnable {
	
	private static final long REFRESH_INTERVAL_MS = 5000;
	private Boolean isRunning = true;
	
	

	public ServerRefreshSensorData() {
		super();
	}



	@Override
	public void run() {
		while(isRunning == true){
			try {
				
				Thread.sleep(REFRESH_INTERVAL_MS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Refresh sensor data");
			ServerSensorService.getInstance().RefreshSensorInfo();
			System.out.println("Refresh sensor data done");
		}

	}

}
