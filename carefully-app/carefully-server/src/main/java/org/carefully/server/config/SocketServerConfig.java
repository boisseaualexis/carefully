package org.carefully.server.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SocketServerConfig {

    static private SocketServerConfig _instance = null;
    static private String port = null;
    static private String host = null;
	 
    private SocketServerConfig(){
    	InputStream file = null;
    	Properties props = new Properties();

	    try{
	        file = getClass().getResourceAsStream("/socketserver.properties");
	        props.load(file);
	        host = props.getProperty("HOST");
	        port = props.getProperty("PORT");
	       } 
	    catch(Exception e){
	        System.out.println("error" + e);
	       }
	    finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					file=null;
				}
			}
		}
    }
	
	static public SocketServerConfig instance(){
        if (_instance == null) {
            _instance = new SocketServerConfig();
        }
        return _instance;
    }
	
    public static String getPort() {
		return port;
	}

	public static String getHost() {
		return host;
	}
}