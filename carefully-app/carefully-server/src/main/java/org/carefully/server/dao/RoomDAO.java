package org.carefully.server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.carefully.commons.bean.Room;

public class RoomDAO extends DAO<Room> {

	public RoomDAO(Connection cn) {
		super(cn);
	}

	@Override
	public Room find(int id) throws SQLException {
		Room room = new Room();
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		      ).executeQuery(
		        "SELECT * FROM room WHERE id = "+id
		      );
		result.next();
		room.setName_room(result.getString("name_room").trim());
		return room;
	}

	@Override
	public ResultSet findAll() throws SQLException {
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		      ).executeQuery(
		    	"SELECT * FROM room"
		      );
		return result;
	}

	@Override
	public boolean create(Room obj) throws SQLException {
		// NOT USED
		return false;
	}

	@Override
	public boolean delete(Room obj) {
		// NOT USED
		return false;
	}

	@Override
	public boolean update(Room obj) {
		// NOT USED
		return false;
	}

}
