package org.carefully.server.app;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.carefully.commons.bean.Room;
import org.carefully.commons.bean.Sensor;
import org.carefully.commons.bean.SensorType;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.server.dao.SensorDAO;
import org.carefully.server.db.connection.pool.ConnectionPool;
import org.carefully.server.service.ServerRoomService;
import org.carefully.server.service.ServerSensorService;
import org.carefully.server.service.ServerSensorTypeService;

public class RequestHandler implements Runnable {
	
	private final DataInputStream is; 
    private final DataOutputStream os;
	private final Socket sckClient;
	private final Connection cnClient;

	public RequestHandler(Socket client, Connection cn) throws IOException {
		this.sckClient = client;
		this.cnClient = cn;
		this.is = new DataInputStream(sckClient.getInputStream());
		this.os = new DataOutputStream(sckClient.getOutputStream());
	}

	@Override
	public void run() {
		
		Request outputRequest = new Request();
		outputRequest.setRequest("fail");
		outputRequest.setSuccess(Boolean.FALSE);
		String response;
		
		try {
			String inputFile = is.readUTF();
			System.out.println("Request received : "+inputFile);
	        Request inputRequest = new Request();
			inputRequest = SerializationDeserialization.deserialization(inputFile);
			
			switch(inputRequest.getRequest()) {
			
			case "select-sensor-all" :
				List<Sensor> listSensors = ServerSensorService.getInstance().findLastSensorInfo();
				
				if(!(listSensors==null)) {
					for (Sensor sensor : listSensors) {
						outputRequest.setObject(sensor);
						outputRequest.setRequest("sensor-next");
						os.writeUTF(SerializationDeserialization.serialization(outputRequest));
					}
					outputRequest = new Request();
					outputRequest.setRequest("sensor-end");
					outputRequest.setSuccess(Boolean.TRUE);
				}
				
				response = SerializationDeserialization.serialization(outputRequest); //sensor-end or fail
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "select-sensor-type-all" :
				List<SensorType> listSensorTypes = ServerSensorTypeService.getInstance().findLastSensorTypeInfo();
				
				if(!(listSensorTypes==null)) {
					for (SensorType sensorType : listSensorTypes) {
						outputRequest.setObject(sensorType);
						outputRequest.setRequest("sensor-type-next");
						os.writeUTF(SerializationDeserialization.serialization(outputRequest));
					}
					outputRequest = new Request();
					outputRequest.setRequest("sensor-type-end");
					outputRequest.setSuccess(Boolean.TRUE);
				}
				
				response = SerializationDeserialization.serialization(outputRequest); //sensor-type-end or fail
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "select-room-all" :
				List<Room> listRooms = ServerRoomService.getInstance().findLastRoomInfo();
				
				if(!(listRooms==null)) {
					for (Room room : listRooms) {
						outputRequest.setObject(room);
						outputRequest.setRequest("room-next");
						os.writeUTF(SerializationDeserialization.serialization(outputRequest));
					}
					outputRequest = new Request();
					outputRequest.setRequest("room-end");
					outputRequest.setSuccess(Boolean.TRUE);
				}
				
				response = SerializationDeserialization.serialization(outputRequest); //room-end or fail
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "install-sensor" :
				boolean resultInstall = false;
				Sensor sensorToInstall = new Sensor();
				sensorToInstall = SerializationDeserialization.deserialization(inputRequest.getObject(), sensorToInstall);
				
				RequestChecker checkerInstall = new RequestChecker(ServerSensorService.getInstance().findLastSensorInfo(), sensorToInstall);
				HashMap<String, String> resultCheckInstall = checkerInstall.checkInstall();
				if(resultCheckInstall.get("validation")=="1") {
					SensorDAO sensorDaoInstall = new SensorDAO(cnClient);
					resultInstall = sensorDaoInstall.update(sensorToInstall);
				}
				
				outputRequest = new Request();
				outputRequest.setRequest("install-sensor-end");
				outputRequest.setObject(resultCheckInstall);
				outputRequest.setSuccess(resultInstall);
				
				response = SerializationDeserialization.serialization(outputRequest); //room-end or fail
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "uninstall-sensor" :
				Sensor sensorToUninstall = new Sensor();
				sensorToUninstall = SerializationDeserialization.deserialization(inputRequest.getObject(), sensorToUninstall);
				
				/*RequestChecker checkerUninstall = new RequestChecker();
				if(!checkerUninstall.checkUninstall(sensorToUninstall)) {
					break;
				}*/
				
				SensorDAO sensorDaoUninstall = new SensorDAO(cnClient);
				boolean resultUnistall = sensorDaoUninstall.updateUninstall(sensorToUninstall);
				
				outputRequest = new Request();
				outputRequest.setRequest("uninstall-sensor-end");
				
				outputRequest.setSuccess(resultUnistall);
				
				response = SerializationDeserialization.serialization(outputRequest); //room-end or fail
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "insert-sensor" :
				Sensor sensor = new Sensor();
				sensor = SerializationDeserialization.deserialization(inputRequest.getObject(), sensor);
				SensorDAO sensorDao2 = new SensorDAO(cnClient);
				sensorDao2.create(sensor);
				
				outputRequest.setRequest("sensor-end");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			}
			
			sckClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			outputRequest = new Request();
			outputRequest.setRequest("fail");
			outputRequest.setSuccess(Boolean.FALSE);
			response = SerializationDeserialization.serialization(outputRequest); //room-end or fail
			System.out.println("Response transmited : "+response);
			try {
				os.writeUTF(response);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		finally {
			ConnectionPool.free(cnClient);
		}
	}

}
