package org.carefully.server.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.carefully.commons.bean.Room;
import org.carefully.server.dao.RoomDAO;
import org.carefully.server.db.connection.pool.ConnectionDB;

public class ServerRoomService {
	
	private static ServerRoomService instance;
	
	ConnectionDB connectionDB = ConnectionDB.instance();

	private List<Room> rooms;

	private ServerRoomService() {
		selectAllRooms();
	}
	
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		
		Connection cnDB = connectionDB.getConnection();
		return cnDB;
	}
	
	private void selectAllRooms() {
		List<Room> listRooms = new ArrayList<Room>();
		try {
			RoomDAO roomDao = new RoomDAO(getConnection());
			ResultSet result = roomDao.findAll();
			while (result.next()) {
				Room room = new Room();
				
				room.setName_room(result.getString("name_room").trim());
				room.setX_pos_room(result.getInt("x_pos_room"));
				room.setY_pos_room(result.getInt("y_pos_room"));
				room.setWidth_room(result.getInt("width_room"));
				room.setHeight_room(result.getInt("height_room"));
				listRooms.add(room);
			}
			this.rooms = listRooms;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static synchronized ServerRoomService getInstance() {
		if (instance == null) {
			instance = new ServerRoomService();
		}
		return instance;
	}

	public synchronized List<Room> findLastRoomInfo() {
		//selectAllRooms();
		return rooms;
	}
}
