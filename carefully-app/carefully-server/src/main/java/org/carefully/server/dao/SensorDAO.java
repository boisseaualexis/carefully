package org.carefully.server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.carefully.commons.bean.Sensor;
import org.carefully.server.service.ServerSensorService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import inet.ipaddr.AddressStringException;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import inet.ipaddr.IncompatibleAddressException;

public class SensorDAO extends DAO<Sensor> {

	public SensorDAO(Connection cn) {
		super(cn);
	}
	
	@Override
	public boolean create(Sensor obj) throws SQLException {
		int result = this.connection.createStatement().executeUpdate(
		    	"INSERT INTO sensor (ip_address, mac_address, fk_state, fk_type, fk_room) VALUES ("
		    	+ "'"+obj.getIp_address() + "', "
		    	+ "'"+obj.getMac_address() + "', "
		    	+ "0" + ", "
		    	+ "'"+obj.getFk_type() + "', "
		    	+ "'"+obj.getFk_room()
		    	+"')"
		      );
		if (result==1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(Sensor obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Sensor obj) throws SQLException {
		String hallwayCondition = "";
		if (obj.getFk_room().equalsIgnoreCase("hallway")) {
			if (obj.getY_pos()<310) {
				hallwayCondition = "and y_pos_room=310";
			}
			else {
				hallwayCondition = "and y_pos_room=25";
			}
		}
		String ipAddress = getIPAddressAvailable();
		int result = this.connection.createStatement().executeUpdate(
				"UPDATE sensor "
				+ "SET ip_address='"+ipAddress+"', "
				+ "x_pos="+obj.getX_pos()+", "
				+ "y_pos="+obj.getY_pos()+", "
				+ "value=0, "
				+ "fk_room=(select id_room from room where name_room='"+obj.getFk_room().toLowerCase()+"' "+hallwayCondition+"), "
				+ "fk_state=2 "
				+ "WHERE mac_address='"+obj.getMac_address()+"';"
		      );
		if (result==1) {
			return true;
		}
		return false;
	}

	private String getIPAddressAvailable() {
		String newIPAddress = null;
		List<Sensor> listSensors = ServerSensorService.getInstance().findLastSensorInfo();
		Iterator<? extends IPAddress> ipAddressIterator = null;
		IPAddressString ipAddressRange = new IPAddressString("10.10.10.17-255");
		List<String> ipAddressSensors = new ArrayList<String>();
		for (Sensor sensor : listSensors) {
			ipAddressSensors.add(sensor.getIp_address());
		}
		
		try {
			ipAddressIterator = ipAddressRange.toAddress().iterator();
			while (ipAddressIterator.hasNext()) {
				String ipAddressTest = ipAddressIterator.next().toString().trim();
				if(ipAddressSensors.contains(ipAddressTest)) {
					continue;
				}
				newIPAddress = ipAddressTest;
				break;
			}
			return newIPAddress;
		} catch (AddressStringException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IncompatibleAddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Sensor find(int id) throws SQLException {
		Sensor sensor = new Sensor();
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		      ).executeQuery(
		        "SELECT * FROM sensor WHERE id = "+id
		      );
		result.next();
		sensor.setMac_address(result.getString("mac_address").trim());
		sensor.setFk_room(result.getString("fk_room").trim());
		sensor.setFk_state(result.getString("fk_state").trim());
		sensor.setFk_type(result.getString("fk_type").trim());
		if(!(sensor.getFk_state().equalsIgnoreCase("1"))) { // installed (NOT 'non installe')
			sensor.setIp_address(result.getString("ip_address").trim());
			sensor.setX_pos(result.getInt("x_pos"));
			sensor.setY_pos(result.getInt("y_pos"));
			if(!(sensor.getFk_state().equalsIgnoreCase("5"))) { // down (NOT 'en panne')
				sensor.setValue(Integer.valueOf(result.getString("value").trim()));
			}
		}
		return sensor;
	}

	@Override
	public ResultSet findAll() throws SQLException {
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		      ).executeQuery(
		    	"SELECT * FROM sensor"
		      );
		return result;
	}
	
	public ResultSet findAllWithJoin() throws SQLException {
		ResultSet result = this.connection.createStatement(
		        ResultSet.TYPE_SCROLL_INSENSITIVE, 
		        ResultSet.CONCUR_READ_ONLY
		        //"SELECT * FROM sensor"
		      ).executeQuery(
		    		  "SELECT ip_address, mac_address, x_pos, y_pos, value, name_sensor_state, name_sensor_type, name_room "
		    		  + "FROM public.sensor "
		    		  + "join public.room on fk_room=id_room "
		    		  + "join public.sensor_state on fk_state=id_sensor_state "
		    		  + "join public.sensor_type on fk_type=id_sensor_type "
		    		  + ";"
		      );
		return result;
	}

	public boolean updateUninstall(Sensor sensorToUninstall) throws SQLException {
		int result = this.connection.createStatement().executeUpdate(
				"UPDATE sensor "
				+ "SET ip_address=null, "
				+ "x_pos=null, "
				+ "y_pos=null, "
				+ "value=null, "
				+ "fk_room=0, "
				+ "fk_state=6 "
				+ "WHERE mac_address='"+sensorToUninstall.getMac_address()+"';"
		      );
		if (result==1) {
			return true;
		}
		return false;
	}
}
