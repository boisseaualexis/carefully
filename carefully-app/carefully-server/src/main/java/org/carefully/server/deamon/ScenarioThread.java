package org.carefully.server.deamon;

import java.io.IOException;
import java.sql.Connection;

public abstract class ScenarioThread implements Runnable {
	
	protected final Connection cnDB;
	
	public ScenarioThread(Connection cn) throws IOException {
		super();
		this.cnDB = cn;
	}

	@Override
	public abstract void run();

}
