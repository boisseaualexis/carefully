package org.carefully.server.deamon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class FireThread extends ScenarioThread {

	public FireThread(Connection cn) throws IOException {
		super(cn);
	}

	@Override
	public void run() {
		Thread allGoodThread;
		try {
			allGoodThread = new Thread(new AllGoodThread(cnDB));
			allGoodThread.start();
			allGoodThread.join();
		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
		}
		
		try {
			int result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=3 "
					+ "WHERE fk_room=1 and fk_type=6;"
			      );
			Thread.sleep(5000);
			
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=4 "
					+ "WHERE fk_room=1 and fk_type=6;"
			      );
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=3 "
					+ "WHERE fk_room in (2,5) and fk_type=1;"
			      );
			Thread.sleep(5000);
			
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=5 "
					+ "WHERE fk_room=1 and fk_type in (6,5);"
			      );
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=4 "
					+ "WHERE fk_room in (2,5) and fk_type in (1,2,3);"
			      );
			
			Thread.sleep(5000);
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=3 "
					+ "WHERE fk_room in (3,4,6) and fk_type in (1,2,3);"
			      );
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=5 "
					+ "WHERE fk_room=5 and fk_type=5;"
			      );
			
			Thread.sleep(5000);
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=5 "
					+ "WHERE fk_room in (2,3,5);"
			      );
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=4 "
					+ "WHERE fk_room in (3,4,6) and fk_type in (1,2,3);"
			      );
			
			Thread.sleep(5000);
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=5 "
					+ "WHERE fk_room>0"
			      );
			
		} catch (SQLException | InterruptedException e) {
			e.printStackTrace();
		}

	}

}
