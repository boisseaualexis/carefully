package org.carefully.server.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DBConfig {

    static private DBConfig _instance = null;
    static private String driver = null;
    static private String url = null;
    static private String user = null;
    static private String pw = null;
	 
    private DBConfig(){
    	InputStream file = null;
    	Properties props = new Properties();

	    try{
	        file = getClass().getResourceAsStream("/db.properties");
	        props.load(file);
	        driver = props.getProperty("DRIVER");
	        url = props.getProperty("URL");
	        user = props.getProperty("USER");
	        pw = props.getProperty("PW");
	       } 
	    catch(Exception e){
	        System.out.println("error" + e);
	       }
	    finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					file=null;
				}
			}
		}
    }
	
	static public DBConfig instance(){
        if (_instance == null) {
            _instance = new DBConfig();
        }
        return _instance;
    }

	public static String getDriver() {
		return driver;
	}

	public static String getUrl() {
		return url;
	}

	public static String getUser() {
		return user;
	}

	public static String getPw() {
		return pw;
	}
}