package org.carefully.server.app;

import org.carefully.server.deamon.DaemonServer;
import org.carefully.server.service.ServerRefreshSensorData;

public class AppServer 
{
	public static void main(String[] args){
		ServerAcceptor sa = new ServerAcceptor();
	    sa.open();
	    
	    Thread refreshSensorDataThread = new Thread(new ServerRefreshSensorData());
        refreshSensorDataThread.start();
	    
	    DaemonServer ds = new DaemonServer();
	    ds.open();
	    
	    System.out.println("Server started");
	}
}
