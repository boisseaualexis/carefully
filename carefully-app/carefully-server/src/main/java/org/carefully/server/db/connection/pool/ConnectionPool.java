package org.carefully.server.db.connection.pool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Vector;

import org.carefully.server.config.ConnectionPoolConfig;

public class ConnectionPool {
		
		//Initialize Pool configuration
		ConnectionPoolConfig connectionPoolConfig = ConnectionPoolConfig.instance();
	
		//Initialize ConnectionDB
		ConnectionDB connectionDB = ConnectionDB.instance();
		
		static private ConnectionPool _instance = null;
		
		//Initialize default values
		static private Integer capacity = 3;
		@SuppressWarnings("unused")
		static private Integer maxCapacity = 3;
		
		private static Vector<Connection> pool = null;
		private static Vector<Connection> poolUsed = null;
		
	    private ConnectionPool() {
			super();
			capacity = Integer.valueOf(ConnectionPoolConfig.getCapacity());
			maxCapacity = Integer.valueOf(ConnectionPoolConfig.getMaxCapacity());
			pool = new Vector<Connection>(capacity);
			poolUsed = new Vector<Connection>(capacity);
			fill();
		}
	    
	    // Fill pool with connections
		private void fill() {
			synchronized(this) {
				for(int k=1; k<=capacity; k++) {
					try {
						Connection cn = ConnectionDB.getConnection();
						pool.add(cn);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}

		public static synchronized Connection getConnection() throws ConnectionException {
			Connection cn = null;
			int loop = 0;
			
			while (cn==null && loop<5) {
				
				try {	 
					cn = pool.remove(0);
					poolUsed.add(cn);
				}
				catch(ArrayIndexOutOfBoundsException e){
					/*
					try{
						if(poolUsed.size()<maxCapacity) {
							cn = ConnectionDB.getConnection();
							poolUsed.add(cn);
						
						} else {
							System.out.println("Error : maximum number of connections reached");
							throw new ConnectionException("maximum number of connections reached");
						}
					} 
					catch (SQLException | ClassNotFoundException f) {
						throw new ConnectionException("db connection unavailable");
					}
					*/
					System.out.println("Error : maximum number of db connections reached");
					loop++;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			
			if (cn==null) {
				throw new ConnectionException("pool unavailable");
			}
			System.out.println("Connection : "+cn.toString()+" taken from the pool");
			return cn;
	    }
		
	    public static synchronized void free(Connection cn) {
				poolUsed.remove(cn);
		    	pool.add(cn);
		    	System.out.println("Connection : "+cn.toString()+" given back to the pool");
		}
	    
	    public static ConnectionPool instance(){
	        if (_instance == null) {
	            _instance = new ConnectionPool();
	        }
	        return _instance;
	    }
}
