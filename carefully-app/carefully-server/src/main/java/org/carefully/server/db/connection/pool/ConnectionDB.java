package org.carefully.server.db.connection.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.carefully.server.config.DBConfig;

public class ConnectionDB {
	
	//Initialize DB connection configuration
	DBConfig dbConfig = DBConfig.instance();
	
	static private ConnectionDB _instance = null;
	static private String driver = null;
	static private String url = null;
	static private String user = null;
	static private String pw = null;
    
    private ConnectionDB() {
		super();
		driver = DBConfig.getDriver();
		url = DBConfig.getUrl();
		user = DBConfig.getUser();
		pw = DBConfig.getPw();
	}

	static public Connection getConnection() throws ClassNotFoundException, SQLException {
    	Class.forName(driver);
    	Connection cn = DriverManager.getConnection(url,user,pw);
    	return cn;
    }
    
    static public ConnectionDB instance(){
        if (_instance == null) {
            _instance = new ConnectionDB();
        }
        return _instance;
    }
}
