package org.carefully.server.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.carefully.commons.bean.Sensor;
import org.carefully.server.dao.SensorDAO;
import org.carefully.server.db.connection.pool.ConnectionDB;

public class ServerSensorService {
	
	private static ServerSensorService instance;
	
	private static final String NOT_INSTALLED = "non installe";
	private static final String UNINSTALLED = "desinstalle";
	private static final String DOWN = "panne";
	
	ConnectionDB connectionDB = ConnectionDB.instance();

	private List<Sensor> sensors;

	private ServerSensorService() {
		selectAllSensors();
	}
	
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		
		Connection cnDB = connectionDB.getConnection();
		return cnDB;
	}
	
	private void selectAllSensors() {
		List<Sensor> listSensors = new ArrayList<Sensor>();
		try {
			SensorDAO sensorDao = new SensorDAO(getConnection());
			//ResultSet result = sensorDao.findAll();
			ResultSet result = sensorDao.findAllWithJoin();
			while (result.next()) {
				Sensor sensor = new Sensor();
				/*sensor.setFk_room(result.getString("fk_room").trim());
				sensor.setFk_state(result.getString("fk_state").trim());
				sensor.setFk_type(result.getString("fk_type").trim());*/
				
				sensor.setFk_room(result.getString("name_room").trim());
				sensor.setFk_state(result.getString("name_sensor_state").trim());
				sensor.setFk_type(result.getString("name_sensor_type").trim());
				sensor.setMac_address(result.getString("mac_address").trim());
				
				if(!(sensor.getFk_state().equalsIgnoreCase(NOT_INSTALLED) || sensor.getFk_state().equalsIgnoreCase(UNINSTALLED))) {
					sensor.setIp_address(result.getString("ip_address").trim());
					sensor.setX_pos(result.getInt("x_pos"));
					sensor.setY_pos(result.getInt("y_pos"));
					if(!(sensor.getFk_state().equalsIgnoreCase(DOWN))) {
						sensor.setValue(Integer.valueOf(result.getString("value").trim()));
					}
				}
				listSensors.add(sensor);
			}
			this.sensors = listSensors;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static synchronized ServerSensorService getInstance() {
		if (instance == null) {
			instance = new ServerSensorService();
		}
		return instance;
	}

	public synchronized List<Sensor> findLastSensorInfo() {
		//selectAllSensors();
		return sensors;
	}

	protected void RefreshSensorInfo() {
		selectAllSensors()	;	
	}
}
