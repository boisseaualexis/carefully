package org.carefully.server.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.carefully.commons.bean.Sensor;
import org.carefully.commons.bean.SensorType;
import org.carefully.server.service.ServerSensorTypeService;

public class RequestChecker {
	
	private static final int MAX_SENSOR_OF_ONE_TYPE_IN_ROOM = 2;
	private static final int MAX_SENSORS_IN_ROOM = 7;
	private static final int MIN_RANGE = 100;
	private static final Integer RADIUS = 50;
	private static final String NO_SENSOR_ONTO_ANOTHER = "Le capteur est positionné trop proche d'un autre capteur";
	private static final String NO_SMOKE_SENSOR_INTO_KITCHEN = "Un détecteur de fumée ne peut pas être installé dans la cuisine";
	private static final String NO_MORE_SENSOR_OF_TYPE_INTO_ROOM = "La pièce contient déjà trop de capteurs (2) de type : ";
	private static final String NO_MORE_SENSOR_INTO_ROOM = "La pièce contient déjà trop de capteurs (7)";
	private static final String NO_SENSOR_INTO_SCOPE_OF_ANOTHER_OF_SAME_TYPE = "Le capteur est positionné dans l'espace déjà couvert par un autre capteur de même type : ";
	Sensor sensorToCheck;
	List<Sensor> sensorsInSameRoom;
	HashMap<String, String> resultCheck;

	protected RequestChecker(List<Sensor> listAllSensors, Sensor sensorToCheck) {
		super();
		this.sensorToCheck = findByMacAddress(listAllSensors, sensorToCheck.getMac_address());
		this.sensorsInSameRoom = findSensorsInSameRoom(listAllSensors, sensorToCheck.getFk_room());
		this.sensorToCheck.setX_pos(sensorToCheck.getX_pos());
		this.sensorToCheck.setY_pos(sensorToCheck.getY_pos());
		this.sensorToCheck.setFk_room(sensorToCheck.getFk_room());
	}
	
	private List<Sensor> findSensorsInSameRoom(List<Sensor> listAllSensors, String fk_room) {
		List<Sensor> result = new ArrayList<Sensor>();
		for (Sensor sensorTest : listAllSensors) {
			if (sensorTest.getFk_room().equalsIgnoreCase(fk_room)) {

				result.add(sensorTest);
			}
		}
		return result;
	}

	private Sensor findByMacAddress(List<Sensor> listAllSensors, String macAddress) {
		Sensor result = new Sensor();
		for (Sensor sensorTest : listAllSensors) {
			if (sensorTest.getMac_address().equalsIgnoreCase(macAddress)) {
				
				try {
					result = sensorTest.clone();
				} catch (CloneNotSupportedException e) {
					result = sensorTest;
				}
				break;
			}
		}
		return result;
	}

	public HashMap<String, String> checkInstall() {
		resultCheck = new HashMap<String, String>();
		resultCheck.put("validation", "0");
		
		if (sensorToCheck.getMac_address()==null) {
			resultCheck.put("validation", "0");
			return resultCheck;
		}
		
		boolean check = true;
		check = check && checkInstallQuantityInRoom();
		check = check && checkInstallQuantityInRoomByType(sensorToCheck);
		check = check && checkInstallScope(sensorToCheck);
		if(sensorToCheck.getFk_room().equalsIgnoreCase("kitchen")) {
			check = check && checkInstallSmokeIntoKitchen(sensorToCheck);
		}
		check = check && checkInstallOntoAnotherSensor(sensorToCheck);
		
		if (check) {
			resultCheck.put("validation", "1");
		}
		return resultCheck;
	}
	
	private boolean checkInstallQuantityInRoom() {
		if (sensorsInSameRoom.size()<MAX_SENSORS_IN_ROOM) {
			return true;
		}
		resultCheck.put("quantityinroom", NO_MORE_SENSOR_INTO_ROOM);
		return false;
	}

	private boolean checkInstallQuantityInRoomByType(Sensor sensorToCheck) {
		int countSameType = 1;
		for (Sensor sensorTest : sensorsInSameRoom) {
			if(sensorTest.getFk_type().equalsIgnoreCase(sensorToCheck.getFk_type())) {
				countSameType++;
			}
			if (countSameType>MAX_SENSOR_OF_ONE_TYPE_IN_ROOM) {
				resultCheck.put("quantityinroombytype", NO_MORE_SENSOR_OF_TYPE_INTO_ROOM+sensorToCheck.getFk_type());
				return false;
			}
		}
		return true;
	}

	private boolean checkInstallScope(Sensor sensorToCheck) {
		int range = MIN_RANGE;
		
		List<SensorType> listSensorTypes = ServerSensorTypeService.getInstance().findLastSensorTypeInfo();
		for(SensorType typeTest : listSensorTypes) {
			if (typeTest.getName_sensor_type().equalsIgnoreCase(sensorToCheck.getFk_type())) {
				range = typeTest.getRange_sensor_type()*10;
			}
		}
		
		for (Sensor sensorTest : sensorsInSameRoom) {
			if(sensorTest.getFk_type().equalsIgnoreCase(sensorToCheck.getFk_type())) {
				int borderTop = sensorTest.getX_pos()+range;
				int borderDown = sensorTest.getX_pos()-range;
				int borderRight = sensorTest.getY_pos()+range;
				int borderLeft = sensorTest.getY_pos()-range;
				if ((sensorToCheck.getX_pos()<borderTop) && (sensorToCheck.getX_pos()>borderDown) && (sensorToCheck.getY_pos()<borderRight) && (sensorToCheck.getY_pos()>borderLeft)) {
					resultCheck.put("scopeofanother", NO_SENSOR_INTO_SCOPE_OF_ANOTHER_OF_SAME_TYPE+sensorTest.getMac_address());
					return false;
				}
			}	
		}
		return true;
	}

	private boolean checkInstallSmokeIntoKitchen(Sensor sensorToCheck) {
		if (sensorToCheck.getFk_type().equalsIgnoreCase("fumee")) {
			resultCheck.put("smokeintokitchen", NO_SMOKE_SENSOR_INTO_KITCHEN);
			return false;
		}
		return true;
	}

	private boolean checkInstallOntoAnotherSensor(Sensor sensorToCheck) {
		for (Sensor sensorTest : sensorsInSameRoom) {
			int borderTop = sensorTest.getX_pos()+RADIUS;
			int borderDown = sensorTest.getX_pos()-RADIUS;
			int borderRight = sensorTest.getY_pos()+RADIUS;
			int borderLeft = sensorTest.getY_pos()-RADIUS;
			if ((sensorToCheck.getX_pos()<borderTop) && (sensorToCheck.getX_pos()>borderDown) && (sensorToCheck.getY_pos()<borderRight) && (sensorToCheck.getY_pos()>borderLeft)) {
				resultCheck.put("ontoanother", NO_SENSOR_ONTO_ANOTHER);
				return false;
			}
		}
		return true;
	}
}
