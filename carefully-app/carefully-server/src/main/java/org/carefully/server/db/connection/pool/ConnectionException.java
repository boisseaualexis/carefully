package org.carefully.server.db.connection.pool;

public class ConnectionException extends Exception {

	public ConnectionException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -7392699062040783976L;

}
