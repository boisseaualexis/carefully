package org.carefully.server.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.carefully.commons.bean.SensorType;
import org.carefully.server.dao.SensorTypeDAO;
import org.carefully.server.db.connection.pool.ConnectionDB;

public class ServerSensorTypeService {
	
	private static ServerSensorTypeService instance;
	
	ConnectionDB connectionDB = ConnectionDB.instance();

	private List<SensorType> sensorTypes;

	private ServerSensorTypeService() {
		selectAllSensorTypes();
	}
	
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		
		Connection cnDB = connectionDB.getConnection();
		return cnDB;
	}
	
	private void selectAllSensorTypes() {
		List<SensorType> listSensorTypes = new ArrayList<SensorType>();
		try {
			SensorTypeDAO sensorTypeDao = new SensorTypeDAO(getConnection());
			ResultSet result = sensorTypeDao.findAll();
			while (result.next()) {
				SensorType sensorType = new SensorType();
				
				sensorType.setName_sensor_type(result.getString("name_sensor_type").trim());
				sensorType.setRange_sensor_type(result.getInt("range_sensor_type"));
				listSensorTypes.add(sensorType);
			}
			this.sensorTypes = listSensorTypes;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static synchronized ServerSensorTypeService getInstance() {
		if (instance == null) {
			instance = new ServerSensorTypeService();
		}
		return instance;
	}

	public synchronized List<SensorType> findLastSensorTypeInfo() {
		//selectAllSensorTypes();
		return sensorTypes;
	}
}
