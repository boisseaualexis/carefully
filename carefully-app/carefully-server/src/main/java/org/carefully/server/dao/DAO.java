package org.carefully.server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DAO<T> {
	  protected Connection connection = null;
	   
	  public DAO(Connection cn){
	    this.connection = cn;
	  }
	   
	  /**
	  * INSERT
	  * @param obj
	  * @return boolean 
	  * @throws SQLException 
	  */
	  public abstract boolean create(T obj) throws SQLException;

	  /**
	  * DELETE
	  * @param obj
	  * @return boolean 
	  */
	  public abstract boolean delete(T obj) throws SQLException;

	  /**
	  * UPDATE
	  * @param obj
	  * @return boolean
	  */
	  public abstract boolean update(T obj) throws SQLException;

	  /**
	  * SELECT
	  * @param id
	  * @return T
	  * @throws SQLException 
	  */
	  public abstract T find(int id) throws SQLException;
	  
	  /**
	   * SELECT ALL
	   * @return ResultSet
	   * @throws SQLException 
	   */
	  public abstract ResultSet findAll() throws SQLException;
}