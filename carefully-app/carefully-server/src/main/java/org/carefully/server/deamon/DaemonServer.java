package org.carefully.server.deamon;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;

import org.carefully.server.config.SocketDaemonServerConfig;
import org.carefully.server.db.connection.pool.ConnectionDB;
import org.carefully.server.db.connection.pool.ConnectionPool;

public class DaemonServer {
	
	//Initialize ServerSocket configuration
	SocketDaemonServerConfig socketDaemonServerConfig = SocketDaemonServerConfig.instance();
	
	//Initialize ConnectionDB
	ConnectionDB connectionDB = ConnectionDB.instance(); 
	
	   //Initialize default values
	   private Integer port = 9002;
	   private String host = "localhost";
	   private ServerSocket server = null;
	   private boolean isRunning = true;
	   
	   public DaemonServer(){
		  try {
			  port = Integer.valueOf(SocketDaemonServerConfig.getPort());
		  } catch (Exception e) {
		      e.printStackTrace();
		      System.out.println("Pb Config DaemonServer");
		  }
	      try {
	         server = new ServerSocket(port);
	         System.out.println("DaemonServer on "+host+" listening on port "+port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	   
	   public DaemonServer(String newHost, int newPort){
	      host = newHost;
	      port = newPort;
	      try {
	         server = new ServerSocket(port, 100, InetAddress.getByName(host));
	         System.out.println("DaemonServer on "+host+" listening on port "+port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	   
	   
	   //Starting server
	   public void open(){
	      
	      //In a thread because it is an infinite loop
	      Thread t = new Thread(new Runnable(){
	         public void run(){
	            while(isRunning == true){
	               Connection cnClient = null;
	               try {
	            	  //Get connection to DB
	            	  cnClient = ConnectionDB.getConnection();

	                  //Waiting client connection
	                  Socket sckClient = server.accept();
	                  
	                  //Process client connection in a other thread
	                  System.out.println("Client connection accepted : "+ sckClient.getRemoteSocketAddress());
	                  Thread t = new Thread(new RequestDaemonHandler(sckClient, cnClient));
	                  t.start();
	               } catch (IOException e) {
	            	  ConnectionPool.free(cnClient);
	                  System.out.println("Error : pb socket client deamon");
	                  e.printStackTrace();
	                  
	               } catch (SQLException e) {
					System.out.println("Error : " + e.getMessage());
					e.printStackTrace();
	               } catch (ClassNotFoundException e) {
	            	System.out.println("Error : " + e.getMessage());
					e.printStackTrace();
				}
	            }
	            
	            try {
	               server.close();
	            } catch (IOException e) {
	               e.printStackTrace();
	               server = null;
	            }
	         }
	      });
	      
	      t.start();
	   }
	   
	   public void close(){
	      isRunning = false;
	   }   
}
