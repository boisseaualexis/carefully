package org.carefully.server.deamon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class AllGoodThread extends ScenarioThread {

	public AllGoodThread(Connection cn) throws IOException {
		super(cn);
	}

	@Override
	public void run() {
		try {
			int result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=2 "
					+ "WHERE fk_room>0 and NOT fk_state=5;"
			      );
			result = this.cnDB.createStatement().executeUpdate(
					"UPDATE sensor "
					+ "SET fk_state=2, " 
					+ "value=0 "
					+ "WHERE fk_state=5;"
			      );
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
