package org.carefully.server.deamon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Connection;
import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.server.db.connection.pool.ConnectionPool;

public class RequestDaemonHandler implements Runnable {
	
	private final DataInputStream is; 
    private final DataOutputStream os;
	private final Socket sckClient;
	private final Connection cnClient;

	public RequestDaemonHandler(Socket client, Connection cn) throws IOException {
		this.sckClient = client;
		this.cnClient = cn;
		this.is = new DataInputStream(sckClient.getInputStream());
		this.os = new DataOutputStream(sckClient.getOutputStream());
	}

	@Override
	public void run() {
		
		Request outputRequest = new Request();
		outputRequest.setRequest("fail");
		outputRequest.setSuccess(Boolean.FALSE);
		String response;
		
		try {
			String inputFile = is.readUTF();
			System.out.println("Request received : "+inputFile);
	        Request inputRequest = new Request();
			inputRequest = SerializationDeserialization.deserialization(inputFile);
			
			switch(inputRequest.getRequest()) {
			
			case "all-good" :
				Thread allGoodThread = new Thread(new AllGoodThread(cnClient));
				allGoodThread.start();
				
				outputRequest.setRequest("all-good-started");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				allGoodThread.join();
				outputRequest.setRequest("all-good-finished");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "apocalypse" :
				Thread apocalypseThread = new Thread(new ApocalypseThread(cnClient));
				apocalypseThread.start();

				outputRequest.setRequest("apocalypse-started");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				apocalypseThread.join();
				outputRequest.setRequest("apocalypse-finished");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "demo" :
				Thread demoThread = new Thread(new DemoThread(cnClient));
				demoThread.start();

				outputRequest.setRequest("demo-started");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				demoThread.join();
				outputRequest.setRequest("demo-finished");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			case "fire-kitchen" :
				Thread fireThread = new Thread(new FireThread(cnClient));
				fireThread.start();

				outputRequest.setRequest("fire-kitchen-started");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				fireThread.join();
				outputRequest.setRequest("fire-finished");
				outputRequest.setSuccess(Boolean.TRUE);
				response = SerializationDeserialization.serialization(outputRequest);
				System.out.println("Response transmited : "+response);
				os.writeUTF(response);
				
				break;
			}
			
			sckClient.close();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		finally {
			ConnectionPool.free(cnClient);
		}
	}

}
