package org.carefully.demonstration.app;

public class AppDemonstration {
	
	public static MainFrame mf;
	public static MainController mc;
	
    public static void main(String[] args){
    	mf = new MainFrame();
    	mc = new MainController(mf);
    	
    }

}
