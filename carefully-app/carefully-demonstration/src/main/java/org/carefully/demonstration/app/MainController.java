package org.carefully.demonstration.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import org.carefully.commons.request.Request;
import org.carefully.commons.request.SerializationDeserialization;
import org.carefully.demonstration.config.SocketClientRequestor;

public class MainController implements ActionListener {
	
	private MainFrame mf;

	public MainController(MainFrame mf) {
		this.mf = mf;
		this.mf.getAllGood().addActionListener(this);
		this.mf.getApocalypse().addActionListener(this);
		this.mf.getDemo().addActionListener(this);
		this.mf.getFireKitchen().addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if(e.getSource().equals(mf.getAllGood())) {
				Socket sck = SocketClientRequestor.instance().getConnection();
				DataInputStream is = new DataInputStream(sck.getInputStream());
		         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
		         
				 Request request = new Request();
				 request.setRequest("all-good");
				 String jsonRequest = SerializationDeserialization.serialization(request);
				 os.writeUTF(jsonRequest);
				 
				 String jsonResponse1 = is.readUTF();
				 Request response = SerializationDeserialization.deserialization(jsonResponse1);
				 String message1 = response.getRequest();
				 JOptionPane.showMessageDialog(mf, message1, "All good", JOptionPane.INFORMATION_MESSAGE);
				 
				 String jsonResponse2 = is.readUTF();
				 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse2);
				 String message2 = finalResponse.getRequest();
				 JOptionPane.showMessageDialog(mf, message2, "All good", JOptionPane.INFORMATION_MESSAGE);
			}
			if(e.getSource().equals(mf.getDemo())) {
				Socket sck = SocketClientRequestor.instance().getConnection();
				DataInputStream is = new DataInputStream(sck.getInputStream());
		         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
		         
				 Request request = new Request();
				 request.setRequest("demo");
				 String jsonRequest = SerializationDeserialization.serialization(request);
				 os.writeUTF(jsonRequest);
				 
				 String jsonResponse1 = is.readUTF();
				 Request response = SerializationDeserialization.deserialization(jsonResponse1);
				 String message1 = response.getRequest();
				 JOptionPane.showMessageDialog(mf, message1, "Demo", JOptionPane.INFORMATION_MESSAGE);
				 
				 String jsonResponse2 = is.readUTF();
				 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse2);
				 String message2 = finalResponse.getRequest();
				 JOptionPane.showMessageDialog(mf, message2, "Demo", JOptionPane.INFORMATION_MESSAGE);
			}
			if(e.getSource().equals(mf.getApocalypse())) {
				Socket sck = SocketClientRequestor.instance().getConnection();
				DataInputStream is = new DataInputStream(sck.getInputStream());
		         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
		         
				 Request request = new Request();
				 request.setRequest("apocalypse");
				 String jsonRequest = SerializationDeserialization.serialization(request);
				 os.writeUTF(jsonRequest);
				 
				 String jsonResponse1 = is.readUTF();
				 Request response = SerializationDeserialization.deserialization(jsonResponse1);
				 String message1 = response.getRequest();
				 JOptionPane.showMessageDialog(mf, message1, "Apocalypse", JOptionPane.INFORMATION_MESSAGE);
				 
				 String jsonResponse2 = is.readUTF();
				 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse2);
				 String message2 = finalResponse.getRequest();
				 JOptionPane.showMessageDialog(mf, message2, "Apocalypse", JOptionPane.INFORMATION_MESSAGE);
			}
			if(e.getSource().equals(mf.getFireKitchen())) {
				Socket sck = SocketClientRequestor.instance().getConnection();
				DataInputStream is = new DataInputStream(sck.getInputStream());
		         DataOutputStream os = new DataOutputStream(sck.getOutputStream());
		         
				 Request request = new Request();
				 request.setRequest("fire-kitchen");
				 String jsonRequest = SerializationDeserialization.serialization(request);
				 os.writeUTF(jsonRequest);
				 
				 String jsonResponse1 = is.readUTF();
				 Request response = SerializationDeserialization.deserialization(jsonResponse1);
				 String message1 = response.getRequest();
				 JOptionPane.showMessageDialog(mf, message1, "Fire in the Kitchen", JOptionPane.INFORMATION_MESSAGE);
				 
				 String jsonResponse2 = is.readUTF();
				 Request finalResponse = SerializationDeserialization.deserialization(jsonResponse2);
				 String message2 = finalResponse.getRequest();
				 JOptionPane.showMessageDialog(mf, message2, "Fire in the Kitchen", JOptionPane.INFORMATION_MESSAGE);
			}
				 
		} catch (UnknownHostException exc) {
			exc.printStackTrace();
		} catch (IOException exc) {
		     exc.printStackTrace();
		    }
	}
}
