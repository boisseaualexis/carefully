package org.carefully.demonstration.config;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClientRequestor {

    static private SocketClientRequestor _instance = null;
    static private Integer port = 9002;
    static private String server = "172.31.254.59";
	 
    private SocketClientRequestor(){
    	SocketClientConfig socketClientConfig = SocketClientConfig.instance();
    	try {
			  port = Integer.valueOf(SocketClientConfig.getPort());
			  server = SocketClientConfig.getServer();
		  } catch (Exception e) {
		      e.printStackTrace();
		      System.out.println("Pb Config");
		  }
    }
	
	static public SocketClientRequestor instance(){
        if (_instance == null) {
            _instance = new SocketClientRequestor();
        }
        return _instance;
    }
	
    public Socket getConnection() {
    	Socket sck = null;
		try {
			sck = new Socket(server, port);
	    	System.out.println("Connected to "+server+" on port "+port);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return sck;
	}

	public static String getServer() {
		return server;
	}
}