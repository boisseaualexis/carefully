package org.carefully.demonstration.app;

import java.awt.Button;
import java.awt.GridLayout;

import javax.swing.JFrame;

public class MainFrame extends JFrame {
	
		private static final long serialVersionUID = 4279328439747866650L;
		
		private Button allGood, apocalypse, demo, fireKitchen;		
		private GridLayout gl;
		
		public MainFrame() {
			super("Scénarios de démonstration");
			gl = new GridLayout(2, 2, 10, 10);
			this.setLayout(gl);
			
			allGood = new Button("Tout va bien");
			apocalypse = new Button("Apocalypse");
			demo = new Button("Remettre en mode démonstration");
			fireKitchen = new Button("Incendie depuis la cuisine");
			
			this.add(allGood);
			this.add(apocalypse);
			this.add(demo);
			this.add(fireKitchen);
			
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setLocation(50,50);
			this.setSize(600, 300);
			this.setVisible(true);
		}

		public Button getAllGood() {
			return allGood;
		}

		public Button getApocalypse() {
			return apocalypse;
		}

		public Button getDemo() {
			return demo;
		}

		public Button getFireKitchen() {
			return fireKitchen;
		}
}
